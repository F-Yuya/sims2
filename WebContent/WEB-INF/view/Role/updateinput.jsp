<%--updateinput.jsp ロール情報更新入力画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="parameter.RoleNames"%>
<%@page import="parameter.SyainParameters"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMSロール情報更新</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body>
	<jsp:include page="../common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="../common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<c:choose>
							<c:when test="${!updateSyainRole.errFlg}">
								<div class="alert alert-info">
									<button class="close" data-dismiss="alert">&times;</button>
									<strong>Info!</strong> 更新する社員のロールの情報を入力し、Updateボタンをクリックしてください。
								</div>
							</c:when>
							<c:otherwise>
								<div class="alert alert-danger">
									<button class="close" data-dismiss="alert">&times;</button>
									<strong>Error!</strong> メッセージの表示された入力項目を確認し、再度入力してください。
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body form-horizontal">
						<form action="SikakuUpdate" name="dialogform" method="post">
							<fieldset>
								<legend>ロール情報更新</legend>
								<div class="progress">
									<div class="progress-bar" style="width: 50.0%;">入力</div>
									<div class="progress-bar progress-bar-default" style="width: 50.0%;">完了</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="syainNo"> 社員番号 </label>
									<div class="col-sm-4">
										<span class="form-control disabled">${syainRole.syainNo}</span>
										<input type="hidden" name="sikakucode" value="${updatesyainRole.syainNo}" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="syainName"> 社員名 </label>
									<div class="col-sm-4">
										<span class="form-control disabled">${syainRole.syainName}</span>
										<input type="hidden" name="sikakucode" value="${updatesyainRole.syainName}" />
									</div>
								</div>
								<c:choose>
									<c:when test="${empty updateSikakuBean.errMsgSikakugroup}">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="role">
												ロール<span class="required">*</span>
											</label>
											<div class="col-sm-4">
													<select id="role" name="role">
													<option id="role" value="USER">ユーザー</option>
													<option id="role" value="MANAGER">マネージャー</option>
													</select>
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="form-group has-error">
											<label class="col-sm-2 control-label" for="role">
												ロール<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="input-group">
													<select id="role" name="role">
													<option id="role" value="USER">ユーザー</option>
													<option id="role" value="MANAGER">マネージャー</option>
													</select>
													<span class="input-group-addon"><i class="glyphicon glyphicon-remove-circle"></i></span>
												</div>
												<span class="help-block"><i class="fa fa-warning"></i>${updateSikakuBean.errMsgSikakugroup}</span>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
							</fieldset>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-10">
										<button type="button" id="staticModalButton" name="button" value="Update" class="btn btn-primary">
											<i class="glyphicon glyphicon-refresh"></i> Update
										</button>
										<button type="submit" name="button" value="Clear" class="btn btn-default">
											<i class="glyphicon glyphicon-remove"></i> Clear
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- モーダルダイアログ -->
		<div class="modal" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true" data-show="true"
			data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">
							<i class="glyphicon glyphicon-warning-sign"></i> 更新確認
						</h4>
					</div>
					<div class="modal-body">ロール情報を更新してよろしいですか？</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary">
							<i class="glyphicon glyphicon-ok"></i> OK
						</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">
							<i class="glyphicon glyphicon-remove"></i> Cancel
						</button>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="../common/footer.jsp" />
	</div>
</body>
</html>