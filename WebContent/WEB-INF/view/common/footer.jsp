<%--footer.jsp 社員情報管理システムログイン後共通フッター--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<footer>
	<div class="container">
		<div class="copy text-center">
			&copy; Copyright 2017 <a href='http://www.hcs.ac.jp' target="_blank">株式会社&nbsp;HCS</a>
		</div>
	</div>
</footer>