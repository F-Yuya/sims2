<%--head.jsp 社員情報管理システム共通（静的インクルード）--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- jQuery UI -->
<link href="css/jquery-ui-1.12.1.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- styles -->
<link href="css/styles.css" rel="stylesheet">
<link href="vendors/form-helpers/css/bootstrap-formhelpers.css" rel="stylesheet">
<link href="vendors/select/bootstrap-select.css" rel="stylesheet">
<link href="css/forms.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.12.4.js"></script>
<!-- jQuery UI -->
<script src="js/jquery-ui-1.12.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.js"></script>
<script src="vendors/form-helpers/js/bootstrap-formhelpers.js"></script>
<script src="vendors/select/bootstrap-select.js"></script>
<script src="js/custom.js"></script>
<script src="js/forms.js"></script>