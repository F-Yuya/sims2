<%--header.jsp 社員情報管理システムログイン後共通ヘッダー--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="header">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<!-- Logo -->
				<div class="logo">
					<h1>
						<a href="Top"><i class="glyphicon glyphicon-info-sign"></i> 社員情報管理システム</a>
					</h1>
				</div>
			</div>
			<div class="col-md-6">
				<div class="navbar navbar-inverse" role="banner">
					<nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
						<ul class="nav navbar-nav">
							<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i> アカウント情報<b
									class="caret"></b>
							</a>
								<ul class="dropdown-menu animated fadeInUp">
									<li>${loginSession.syainNo}</li>
									<li>${loginSession.syainName}</li>
									<li>${loginSession.role}</li>
									<li><a href="Logout"><i class="glyphicon glyphicon-log-out"></i>Logout</a></li>
								</ul></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>