<%--list.jsp 社員情報管理システム一覧画面共通（静的インクルード）--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<link href="vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">
<!-- jQuery UI -->
<script src="js/jquery-ui-1.12.1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="vendors/datatables/js/jquery.dataTables.js"></script>
<script src="vendors/datatables/dataTables.bootstrap.js"></script>
<script src="js/tables.js"></script>