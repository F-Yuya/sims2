<%--leftMenu.jsp 社員情報管理システムログイン後共通メニュー--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="parameter.RoleNames"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="sidebar content-box" style="display: block;">
	<ul class="nav">
		<!-- Main menu -->
		<li class="submenu"><a href="#">
				<i class="glyphicon glyphicon-list"></i> 社員情報管理<span class="caret pull-right"></span>
			</a> <!-- Sub menu -->
			<ul>
				<li><a href="SyainList"><i class="glyphicon glyphicon-list-alt"></i> 社員情報一覧</a></li>
				<c:if test="${RoleNames.ADMIN == loginSession.role}">
					<li><a href="SyainEntry"><i class="glyphicon glyphicon-user"></i> 社員情報登録</a></li>
				</c:if>
			</ul></li>
		<li class="submenu"><a href="#">
				<i class="glyphicon glyphicon-list"></i> 資格情報管理<span class="caret pull-right"></span>
			</a> <!-- Sub menu -->
			<ul>
				<li><a href="SikakuList"><i class="glyphicon glyphicon-list-alt"></i> 資格情報一覧</a></li>
				<c:if test="${RoleNames.USER != loginSession.role}">
					<li><a href="SikakuEntry"><i class="glyphicon glyphicon-book"></i> 資格情報登録</a></li>
				</c:if>
			</ul></li>
		<li class="submenu"><a href="#">
				<i class="glyphicon glyphicon-list"></i> スキル情報管理<span class="caret pull-right"></span>
			</a> <!-- Sub menu -->
			<ul>
				<li><a href="SkillList"><i class="glyphicon glyphicon-list-alt"></i> スキル情報一覧</a></li>
				<c:if test="${RoleNames.USER != loginSession.role}">
					<li><a href="SkillEntry"><i class="glyphicon glyphicon-tags"></i> スキル情報登録</a></li>
				</c:if>
			</ul></li>
		<c:if test="${RoleNames.ADMIN == loginSession.role}">
			<li class="submenu"><a href="#">
					<i class="glyphicon glyphicon-list"></i> ロール情報管理<span class="caret pull-right"></span>
				</a> <!-- Sub menu -->
				<ul>
					<li><a href="RoleList"><i class="glyphicon glyphicon-list-alt"></i> ロール情報一覧</a></li>
				</ul></li>
		</c:if>
	</ul>
</div>