<%--error.jsp ログイン後エラー画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMSエラー</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body>
	<jsp:include page="./common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="./common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-danger">
						<div class="alert alert-danger alert-block">
							<a class="close" data-dismiss="alert" href="#">&times;</a>
							<h4 class="alert-heading">Error!</h4>
							<c:choose>
								<c:when test="${not empty exMsg}">${exMsg}</c:when>
								<c:otherwise>${errMsg}</c:otherwise>
							</c:choose>
							<br /> <br /> お困りの方はシステム管理者までお問い合わせください。
							<address>kanri@xxx.co.jp</address>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="./common/footer.jsp" />
</body>
</html>