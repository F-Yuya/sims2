<%--error.jsp ログイン前エラー画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMSエラー</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body class="login-bg">
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!-- Logo -->
					<div class="logo">
						<h1>
							<a href=""><i class="glyphicon glyphicon-info-sign"></i> 社員情報管理システム</a>
						</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
					<div class="box">
						<div class="content-wrap">
							<div class="alert alert-danger alert-block">
								<a class="close" data-dismiss="alert" href="#">&times;</a>
								<h4 class="alert-heading">Error!</h4>
								${exMsg} <br /> <br /> お困りの方はシステム管理者までお問い合わせください。
								<address>kanri@xxx.co.jp</address>
							</div>
						</div>
						<a href="Login">ログイン画面に戻る</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>