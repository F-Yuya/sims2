<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMSログイン</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body>
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!-- Logo -->
					<div class="logo">
						<h1>
							<a href=""><i class="glyphicon glyphicon-info-sign"></i>
								社員情報管理システム</a>
						</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
					<div class="box">
						<form action="Login" method="post">
							<div class="content-wrap">
							<c:choose>
								<c:when test="${!loginBean.errFlg}">
									<div class="alert alert-info alert-block">
										<a class="close" data-dismiss="alert" href="#">&times;</a>
										<h4 class="alert-heading">Info!</h4>
										社員番号、パスワードを入力し、 <br> Loginボタンをクリックしてください。
									</div>
								</c:when>
								<c:otherwise>
									<div class="alert alert-danger alert-block">
										<a class="close" data-dismiss="alert" href="#">&times;</a>
										<h4 class="alert-heading">Error!</h4>
										社員番号、またはパスワードに誤りがあります。<br />入力項目を確認し、再度ログインしてください。
									</div>
								</c:otherwise>
							</c:choose>
								<div class="form-group">
									<label>社員番号</label> <input type="text" name="syainNo"
										maxlength="5" value="" class="form-control"
										placeholder="半角英数5文字" />
								</div>
								<div class="form-group">
									<label>パスワード</label> <input type="password" name="password"
										maxlength="10" value="" class="form-control"
										placeholder="半角英数4～10文字以内" />
								</div>
								<label> <input type="checkbox" name="remember"
									class="checkbox-inline"> ログイン情報を保存
								</label>
								<div class="action">
									<button type="submit" name="button" value="Login"
										class="btn btn-primary">
										<i class="glyphicon glyphicon-log-in"></i> Login
									</button>
									<button type="submit" name="button" value="Clear"
										class="btn btn-default">
										<i class="glyphicon glyphicon-remove"></i> Clear
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>