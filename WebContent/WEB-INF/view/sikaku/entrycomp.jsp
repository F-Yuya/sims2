<%--entryinput.jsp 資格情報登録完了画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMS資格情報登録</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body>
	<jsp:include page="../common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="../common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">&times;</button>
							<strong>Info!</strong> 資格情報を登録しました。
						</div>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body form-horizontal">
						<fieldset>
							<legend>資格情報登録</legend>
							<div class="progress">
								<div class="progress-bar" style="width: 33.3%;">入力</div>
								<div class="progress-bar" style="width: 33.3%;">確認</div>
								<div class="progress-bar" style="width: 33.3%;">完了</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"> 資格コード </label>
								<div class="col-sm-4">
									<span class="form-control">${sikakuBean.sikakucode}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"> 資格名 </label>
								<div class="col-sm-4">
									<span class="form-control">${sikakuBean.sikakuname}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"> 資格区分 </label>
								<div class="col-sm-4">
									<span class="form-control">${sikakuBean.sikakugroup}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"> </label>
								<div class="col-sm-4">
									<a href="SikakuList"><button type="button" class="btn btn-lg btn-block btn-primary">
											<i class="glyphicon glyphicon-list-alt"></i> 資格情報一覧
										</button> </a>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../common/footer.jsp" />
</body>
</html>