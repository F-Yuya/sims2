<%--entryinput.jsp 資格情報登録確認画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMS資格情報登録</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body>
	<jsp:include page="../common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-sm-2">
				<jsp:include page="../common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">&times;</button>
							<strong>Info!</strong> 登録する資格の情報を確認し、Entryボタンをクリックしてください。
						</div>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body form-horizontal">
						<fieldset>
							<legend>資格情報登録</legend>
							<div class="progress">
								<div class="progress-bar" style="width: 33.3%;">入力</div>
								<div class="progress-bar" style="width: 33.3%;">確認</div>
								<div class="progress-bar progress-bar-default" style="width: 33.3%;">完了</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									資格コード<span class="required">*</span>
								</label>
								<div class="col-sm-4">
									<span class="form-control">${sikakuBean.sikakucode}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									資格名<span class="required">*</span>
								</label>
								<div class="col-sm-4">
									<span class="form-control">${sikakuBean.sikakuname}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
									資格区分<span class="required">*</span>
								</label>
								<div class="col-sm-4">
									<span class="form-control">${sikakuBean.sikakugroup}</span>
								</div>
							</div>
						</fieldset>
						<form action="SikakuEntry" name="dialogform" method="post">
							<div class="form-actions">
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-10">
										<button type="button" id="staticModalButton" name="button" value="Entry" class="btn btn-primary">
											<i class="glyphicon glyphicon-floppy-save"></i> Entry
										</button>
										<button type="submit" name="button" value="Back" class="btn btn-default">
											<i class="glyphicon glyphicon-step-backward"> Back</i>
										</button>
									</div>
								</div>
							</div>
							<input type="hidden" name="sikakucode" value="${sikakuBean.sikakucode}" />
							<input type="hidden" name="sikakuname" value="${sikakuBean.sikakuname}" />
							<input type="hidden" name="sikakugroup" value="${sikakuBean.sikakugroup}" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- モーダルダイアログ -->
	<div class="modal" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true" data-show="true"
		data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">
						<i class="glyphicon glyphicon-warning-sign"></i> 登録確認
					</h4>
				</div>
				<div class="modal-body">資格情報を登録してよろしいですか？</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary">
						<i class="glyphicon glyphicon-ok"></i> OK
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">
						<i class="glyphicon glyphicon-remove"></i> Cancel
					</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../common/footer.jsp" />
</body>
</html>