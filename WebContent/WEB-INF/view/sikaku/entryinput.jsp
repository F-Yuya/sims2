<%--entryinput.jsp 資格情報登録入力画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMS資格情報登録</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body>
	<jsp:include page="../common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="../common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<c:choose>
							<c:when test="${!sikakuBean.errFlg}">
								<div class="alert alert-info">
									<button class="close" data-dismiss="alert">&times;</button>
									<strong>Info!</strong> 登録する資格の情報を入力してください。
								</div>
							</c:when>
							<c:otherwise>
								<div class="alert alert-danger">
									<button class="close" data-dismiss="alert">&times;</button>
									<strong>Error!</strong> メッセージの表示された入力項目を確認し、再度入力してください。
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body form-horizontal">
						<form action="SikakuEntry" method="post">
							<fieldset>
								<legend>資格情報登録</legend>
								<div class="progress">
									<div class="progress-bar" style="width: 33.3%;">入力</div>
									<div class="progress-bar progress-bar-default" style="width: 33.3%;">確認</div>
									<div class="progress-bar progress-bar-default" style="width: 33.3%;">完了</div>
								</div>
								<c:choose>
									<c:when test="${empty SikakuBean.errMsgSikakucode}">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="sikakucode">
												資格コード<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<input type="text" id="sikakucode" name="sikakucode" maxlength="5" value="${sikakuBean.sikakucode}" class="form-control" placeholder="半角数字３文字" />
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="form-group has-error">
											<label class="col-sm-2 control-label" for="sikakucode">
												資格コード<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="input-group">
													<input type="text" id="sikakucode" name="sikakucode" maxlength="5" value="${SikakuBean.sikakucode}" class="form-control" placeholder="半角数字３文字" />
													<span class="input-group-addon"><i class="glyphicon glyphicon-remove-circle"></i></span>
												</div>
												<span class="help-block"><i class="fa fa-warning"></i>${SikakuBean.errMsgSikakucode}</span>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${empty SikakuBean.errMsgSikakuname}">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="sikakuname">
												資格名<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<input type="text" id="sikakuname" name="sikakuname" maxlength="25" value="${SikakuBean.sikakuname}" class="form-control"
													placeholder="全角30文字以内" />
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="form-group has-error">
											<label class="col-sm-2 control-label" for="sikakuname">
												資格名<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="input-group">
													<input type="text" id="sikakuname" name="sikakuname" maxlength="25" value="${sikakuBean.sikakuname}" class="form-control"
														placeholder="全角30文字以内" />
													<span class="input-group-addon"><i class="glyphicon glyphicon-remove-circle"></i></span>
												</div>
												<span class="help-block"><i class="fa fa-warning"></i>${SikakuBean.errMsgSikakuname}</span>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${empty sikakuBean.errMsgSikakugroup}">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="sikakugroup">
												資格区分<span class="required">*</span>
											</label>
											<div class="col-sm-4">
													<select id="sikakugroup" name="sikakugroup" class="form-control">
													<option id="sikakugroup" value="国家試験">国家試験</option>
													<option id="sikakugroup" value="J検">J検</option>
													<option id="sikakugroup" value="ベンダー">ベンダー</option>
													</select>
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="form-group has-error">
											<label class="col-sm-2 control-label" for="sikakugroup">
												資格区分<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="input-group">
													<select id="sikakugroup" name="sikakugroup">
													<option id="sikakugroup" value="国家試験">国家試験</option>
													<option id="sikakugroup" value="J検">J検</option>
													<option id="sikakugroup" value="ベンダー">ベンダー</option>
													</select>
												</div>
												<span class="help-block"><i class="fa fa-warning"></i>${sikakuBean.errMsgSikakugroup}</span>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
							</fieldset>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-10">
										<button type="submit" name="button" value="Submit" class="btn btn-primary">
											<i class="glyphicon glyphicon-step-forward"></i> Submit
										</button>
										<button type="submit" name="button" value="Clear" class="btn btn-default">
											<i class="glyphicon glyphicon-remove"></i> Clear
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../common/footer.jsp" />
</body>
</html>