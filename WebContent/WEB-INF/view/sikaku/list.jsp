<%--top.jsp 資格情報一覧画面--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@page import="parameter.RoleNames"%>
<%@page import="parameter.SikakuParameters"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMS資格情報一覧</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
<%@include file="/WEB-INF/view/common/list.jsp"%>
</head>
<body>
	<jsp:include page="../common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="../common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">&times;</button>
							<strong>Info!</strong> 資格の一覧を表示します。
						</div>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body form-horizontal">
						<fieldset>
							<legend>
								資格情報一覧 <a href="SikakuList">
									<button class="btn btn-default btn-right">
										<i class="glyphicon glyphicon-eye-open"></i> View
									</button>
								</a>
							</legend>
							<table class="table table-striped table-bordered table-hover" id="example">
								<thead>
									<tr>
										<th>資格コード</th>
										<th>資格名</th>
										<th>資格種別</th>
										<c:choose>
											<c:when test="${RoleNames.ADMIN == loginSession.role}">
												<th>廃止状態</th>
												<th></th>
											</c:when>
											<c:when test="${RoleNames.MANAGER == loginSession.role}">
												<th></th>
											</c:when>
											<c:otherwise></c:otherwise>
										</c:choose>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="sikakuBean" items="${sikakuBeanList}">
										<tr>
											<td>${sikakuBean.sikakucode}</td>
											<td>${sikakuBean.sikakuname}</td>
											<td>${sikakuBean.sikakugroup}</td>
											<c:if test="${RoleNames.ADMIN == loginSession.role}">
												<td>${sikakuBean.status}</td>
											</c:if>
											<c:if test="${RoleNames.USER != loginSession.role}">
												<c:choose>
													<c:when test="${SikakuParameters.NOT_DEL == sikakuBean.status}">
														<td><a href="SikakuUpdate?sikakucode=${sikakuBean.sikakucode}">
																<button class="btn btn-primary">
																	<i class="glyphicon glyphicon-refresh"></i> Update
																</button>
														</a></td>
													</c:when>
													<c:otherwise>
														<td></td>
													</c:otherwise>
												</c:choose>
											</c:if>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../common/footer.jsp" />
</body>
</html>