<%--entryinput.jsp スキル情報登録入力画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="parameter.RoleNames"%>
<% String today = (String) request.getAttribute("today"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMSスキル情報登録</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body>
	<jsp:include page="../common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="../common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<c:choose>
							<c:when test="${!sikllBean.errFlg}">
								<div class="alert alert-info">
									<button class="close" data-dismiss="alert">&times;</button>
									<strong>Info!</strong> 登録するスキルの情報を入力してください。
								</div>
							</c:when>
							<c:otherwise>
								<div class="alert alert-danger">
									<button class="close" data-dismiss="alert">&times;</button>
									<strong>Error!</strong> メッセージの表示された入力項目を確認し、再度入力してください。
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body form-horizontal">
						<form action="SkillEntry" method="post">
							<fieldset>
								<legend>スキル情報登録</legend>
								<div class="progress">
									<div class="progress-bar" style="width: 33.3%;">入力</div>
									<div class="progress-bar progress-bar-default" style="width: 33.3%;">確認</div>
									<div class="progress-bar progress-bar-default" style="width: 33.3%;">完了</div>
								</div>
								<c:choose>
									<c:when test="${empty skillBean.errMsgPassword}">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="syainNo">
												社員<span class="required">*</span>
											</label>
											<div class="col-sm-4">
											<select id="syainNo" name="syainNo" class="form-control">
													<c:forEach var="syainList" items="${syainBeanList}">
													<option  value="${syainList.syainNo}"><c:out value="${syainList.syainName}" /></option>
												</c:forEach>
											</select>
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="form-group has-error">
											<label class="col-sm-2 control-label" for="syainNo">
												社員<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="col-sm-4">
												<select id="syainNo" name="syainNo">
													<c:forEach var="syainList" items="${syainBeanList}">
													<option value="${syainList.syainNo}"><c:out value="${syainList.syainName}" /></option>
													</c:forEach>
												</select>
												</div>
												<span class="help-block"><i class="fa fa-warning"></i>${syainBean.errMsgSyainNo}</span>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${empty skillBean.errMsgPassword}">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="password">
												資格<span class="required">*</span>
											</label>
												<div class="col-sm-4">
												<select id="sikakucode" name="sikakucode" class="form-control">
													<c:forEach var="sikakuList" items="${sikakuBeanList}" >
													<option value="${sikakuList.sikakucode}"><c:out value="${sikakuList.sikakuname}" /></option>
													</c:forEach>
												</select>
												</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="form-group has-error">
											<label class="col-sm-2 control-label" for="password">
												資格<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="col-sm-4">
												<c:forEach var="sikakuList" items="${sikakuList}">
													<option disabled="disabled" class="selected" selected="selected" value=""><c:out value="登録する資格を選んでください"/></option>
													<option id="sikakucode" value="${sikakuList.sikakucode}"><c:out value="${sikakuList.sikakuname}" /></option>
												</c:forEach>
												</div>
												<span class="help-block"><i class="fa fa-warning"></i>${syainBean.errMsgPassword}</span>
											</div>
										</div>
									</c:otherwise>
								</c:choose>

								<c:choose>
									<c:when test="${empty sikakuBean.errMsgSikakugroup}">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="sikakudate">
												取得日<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="input-group">
												<input id="sikakudate" name="sikakudate" class="form-control" type="date" value=<%=today%> max=<%=today %>></input>
											</div>
										</div>
								</div>
							</c:when>
									<c:otherwise>
										<div class="form-group has-error">
											<label class="col-sm-2 control-label" for="sikakugroup">
												取得日<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="input-group">
												<input id="sikakudate" name="sikakudate" type="date"></input>
											</div>
												</div>
												<span class="help-block"><i class="fa fa-warning"></i>${sikakuBean.errMsgSikakugroup}</span>
											</div>
									</c:otherwise>
								</c:choose>
							</fieldset>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-10">
										<button type="submit" name="button" value="Submit" class="btn btn-primary">
											<i class="glyphicon glyphicon-step-forward"></i> Submit
										</button>
										<button type="submit" name="button" value="Clear" class="btn btn-default">
											<i class="glyphicon glyphicon-remove"></i> Clear
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../common/footer.jsp" />
</body>
</html>