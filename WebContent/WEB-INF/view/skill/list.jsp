<%--top.jsp スキル情報一覧画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="parameter.RoleNames"%>
<%@page import="parameter.SyainParameters"%>
<%@page import="parameter.SikakuParameters"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMSスキル情報一覧</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
<%@include file="/WEB-INF/view/common/list.jsp"%>
</head>
<body>
	<jsp:include page="../common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="../common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">&times;</button>
							<strong>Info!</strong> スキルの一覧を表示します。
						</div>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body form-horizontal">
						<fieldset>
							<legend>
								スキル情報一覧 <a href="SkillList">
									<button class="btn btn-default btn-right">
										<i class="glyphicon glyphicon-eye-open"></i> View
									</button>
								</a>
							</legend>
							<table class="table table-striped table-bordered table-hover" id="example">
								<thead>
									<tr>
										<th>社員番号</th>
										<th>社員名</th>
										<th>資格コード</th>
										<th>資格名</th>
										<th>資格取得日</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="syainSkillBeanList" items="${syainSkillBeanList}">
										<tr>
											<td>${syainSkillBeanList.syainno}</td>
											<td>${syainSkillBeanList.syainName}</td>
											<td>${syainSkillBeanList.sikakucode}</td>
											<td>${syainSkillBeanList.sikakuName}</td>
											<td>${syainSkillBeanList.sikakudate}</td>
											<c:if test="${RoleNames.USER != loginSession.role}">
														<td><a href="SkillUpdate?syainNo=${syainSkillBeanList.syainno}&sikakucode=${syainSkillBeanList.sikakucode}">
																<button class="btn btn-primary">
																	<i class="glyphicon glyphicon-refresh"></i> Update
																</button>
														</a></td>
											</c:if>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../common/footer.jsp" />
</body>
</html>