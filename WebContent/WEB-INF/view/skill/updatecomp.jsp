<%--entryinput.jsp 社員情報更新完了画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="parameter.RoleNames"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMSスキル情報更新</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body>
	<jsp:include page="../common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="../common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">&times;</button>
							<strong>Info!</strong> スキル情報を更新しました。
						</div>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body form-horizontal">
						<fieldset>
							<legend>スキル情報更新</legend>
							<div class="progress">
								<div class="progress-bar" style="width: 50.0%;">入力</div>
								<div class="progress-bar" style="width: 50.0%;">完了</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="syainNo"> 社員番号 </label>
								<div class="col-sm-4">
									<span class="form-control">${updateSyainSkillBean.syainno}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="syainname"> 社員名 </label>
								<div class="col-sm-4">
									<span class="form-control">${updateSyainSkillBean.syainName}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="syainname"> 資格コード </label>
								<div class="col-sm-4">
									<span class="form-control">${updateSyainSkillBean.sikakucode}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="syainname"> 資格名 </label>
								<div class="col-sm-4">
									<span class="form-control">${updateSyainSkillBean.sikakuName}</span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"> 資格取得日 </label>
								<div class="col-sm-4">
									<div class="input-group">
										<span class="input-group-addon">更新前</span> <span class="form-control">${syainSkillBean.sikakudate}</span>
									</div>
									<div class="input-group">
										<span class="input-group-addon">更新後</span> <span class="form-control">${updateSyainSkillBean.sikakudate}</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"> </label>
								<div class="col-sm-4">
									<a href="SkillList"><button type="button" class="btn btn-lg btn-block btn-primary">
											<i class="glyphicon glyphicon-list-alt"></i> スキル情報一覧
										</button> </a>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../common/footer.jsp" />
</body>
</html>