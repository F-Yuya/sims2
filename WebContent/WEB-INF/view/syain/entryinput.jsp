<%--entryinput.jsp 社員情報登録入力画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMS社員情報登録</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body>
	<jsp:include page="../common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="../common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<c:choose>
							<c:when test="${!syainBean.errFlg}">
								<div class="alert alert-info">
									<button class="close" data-dismiss="alert">&times;</button>
									<strong>Info!</strong> 登録する社員の情報を入力してください。
								</div>
							</c:when>
							<c:otherwise>
								<div class="alert alert-danger">
									<button class="close" data-dismiss="alert">&times;</button>
									<strong>Error!</strong> メッセージの表示された入力項目を確認し、再度入力してください。
								</div>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body form-horizontal">
						<form action="SyainEntry" method="post">
							<fieldset>
								<legend>社員情報登録</legend>
								<div class="progress">
									<div class="progress-bar" style="width: 33.3%;">入力</div>
									<div class="progress-bar progress-bar-default" style="width: 33.3%;">確認</div>
									<div class="progress-bar progress-bar-default" style="width: 33.3%;">完了</div>
								</div>
								<c:choose>
									<c:when test="${empty syainBean.errMsgSyainNo}">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="syainNo">
												社員番号<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<input type="text" id="syainNo" name="syainNo" maxlength="5" value="${syainBean.syainNo}" class="form-control" placeholder="半角英数5文字" />
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="form-group has-error">
											<label class="col-sm-2 control-label" for="syainNo">
												社員番号<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="input-group">
													<input type="text" id="syainNo" name="syainNo" maxlength="5" value="${syainBean.syainNo}" class="form-control" placeholder="半角英数5文字" />
													<span class="input-group-addon"><i class="glyphicon glyphicon-remove-circle"></i></span>
												</div>
												<span class="help-block"><i class="fa fa-warning"></i>${syainBean.errMsgSyainNo}</span>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${empty syainBean.errMsgSyainName}">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="syainName">
												社員名<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<input type="text" id="syainName" name="syainName" maxlength="25" value="${syainBean.syainName}" class="form-control"
													placeholder="全角25文字以内" />
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="form-group has-error">
											<label class="col-sm-2 control-label" for="syainName">
												社員名<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="input-group">
													<input type="text" id="syainName" name="syainName" maxlength="25" value="${syainBean.syainName}" class="form-control"
														placeholder="全角25文字以内" />
													<span class="input-group-addon"><i class="glyphicon glyphicon-remove-circle"></i></span>
												</div>
												<span class="help-block"><i class="fa fa-warning"></i>${syainBean.errMsgSyainName}</span>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${empty syainBean.errMsgPassword}">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="password">
												パスワード<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<input type="text" id="password" name="password" maxlength="10" value="${syainBean.password}" class="form-control"
													placeholder="半角英数4～10文字以内" />
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="form-group has-error">
											<label class="col-sm-2 control-label" for="password">
												パスワード<span class="required">*</span>
											</label>
											<div class="col-sm-4">
												<div class="input-group">
													<input type="text" id="password" name="password" maxlength="10" value="${syainBean.password}" class="form-control"
														placeholder="半角英数4～10文字以内" />
													<span class="input-group-addon"><i class="glyphicon glyphicon-remove-circle"></i></span>
												</div>
												<span class="help-block"><i class="fa fa-warning"></i>${syainBean.errMsgPassword}</span>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
							</fieldset>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-10">
										<button type="submit" name="button" value="Submit" class="btn btn-primary">
											<i class="glyphicon glyphicon-step-forward"></i> Submit
										</button>
										<button type="submit" name="button" value="Clear" class="btn btn-default">
											<i class="glyphicon glyphicon-remove"></i> Clear
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../common/footer.jsp" />
</body>
</html>