<%--top.jsp 社員情報一覧画面--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="parameter.RoleNames"%>
<%@page import="parameter.SyainParameters"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMS社員情報一覧</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
<%@include file="/WEB-INF/view/common/list.jsp"%>
</head>
<body>
	<jsp:include page="../common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="../common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">&times;</button>
							<strong>Info!</strong> 社員の一覧を表示します。
						</div>
					</div>
				</div>
				<div class="content-box-large">
					<div class="panel-body form-horizontal">
						<fieldset>
							<legend>
								社員情報一覧 <a href="SyainList">
									<button class="btn btn-default btn-right">
										<i class="glyphicon glyphicon-eye-open"></i> View
									</button>
								</a>
							</legend>
							<table class="table table-striped table-bordered table-hover" id="example">
								<thead>
									<tr>
										<th>社員番号</th>
										<th>社員名</th>
										<th>パスワード</th>
										<c:choose>
											<c:when test="${RoleNames.ADMIN == loginSession.role}">
												<th>在籍状況</th>
												<th></th>
											</c:when>
											<c:when test="${RoleNames.MANAGER == loginSession.role}">
												<th></th>
											</c:when>
											<c:otherwise></c:otherwise>
										</c:choose>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="syainBean" items="${syainBeanList}">
										<tr>
											<td>${syainBean.syainNo}</td>
											<td>${syainBean.syainName}</td>
											<td>${syainBean.password}</td>
											<c:if test="${RoleNames.ADMIN == loginSession.role}">
												<td>${syainBean.status}</td>
											</c:if>
											<c:if test="${RoleNames.USER != loginSession.role}">
												<c:choose>
													<c:when test="${SyainParameters.NOT_RETIRE == syainBean.status}">
														<td><a href="SyainUpdate?syainNo=${syainBean.syainNo}">
																<button class="btn btn-primary">
																	<i class="glyphicon glyphicon-refresh"></i> Update
																</button>
														</a></td>
													</c:when>
													<c:otherwise>
														<td></td>
													</c:otherwise>
												</c:choose>
											</c:if>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../common/footer.jsp" />
</body>
</html>