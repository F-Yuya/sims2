<%--top.jsp  ログイン後トップページ--%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIMSトップ</title>
<%@include file="/WEB-INF/view/common/head.jsp"%>
</head>
<body>
	<jsp:include page="./common/header.jsp" />
	<div class="page-content">
		<div class="row">
			<div class="col-md-2">
				<jsp:include page="./common/menu.jsp" />
			</div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-12 panel-info">
						<div class="alert alert-info">
							<button class="close" data-dismiss="alert">&times;</button>
							<strong>Info!</strong> メニューから操作を選択してください。
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="./common/footer.jsp" />
</body>
</html>