$(document).ready(function() {
	// Select
	$('.selectpicker').selectpicker();

	$('#staticModalButton').on('click', function() {
		$('#staticModal').modal();
	});

	$('#staticDeleteModalButton').on('click', function() {
		$('#staticDeleteModal').modal();
	});
	// ダイアログ表示直後にフォーカスを設定する
	$('#staticModal').on('shown.bs.modal', function(event) {
		$(this).find('.modal-footer .btn-primary').focus();
	});

	$('#staticDeleteModal').on('shown.bs.modal', function(event) {
		$(this).find('.modal-footer .btn-primary').focus();
	});

	$('#staticModal').on('click', '.modal-footer .btn-primary', function() {
		$('#staticModal').modal('hide');
		// buttonのvalue値を取得する
		var elements = document.getElementsByName("button");
		var value = elements[0].value;
		var submitType = document.createElement("input");
		submitType.setAttribute("type", "hidden");
		submitType.setAttribute("name", "button");
		submitType.setAttribute("value", value);
		document.dialogform.appendChild(submitType);
		document.dialogform.submit();
	});

	$('#staticDeleteModal').on('click', '.modal-footer .btn-primary', function() {
		$('#staticDeleteModal').modal('hide');
		// buttonのvalue値を取得する
		var elements = document.getElementsByName("button");
		var value = elements[1].value;
		var submitType = document.createElement("input");
		submitType.setAttribute("type", "hidden");
		submitType.setAttribute("name", "button");
		submitType.setAttribute("value", value);
		document.dialogform.appendChild(submitType);
		document.dialogform.submit();
	});

	$('#sikakuDate').val($('#datepicker').val());

	// // Tags
	// $("#tags").tags({
	// suggestions: ["alpha", "bravo", "charlie", "delta", "echo", "foxtrot",
	// "golf", "hotel", "india"],
	// tagData: ["juliett", "kilo"]
	// });
	//
	// // Editable
	// $('.editable').editable();
	//
	// // Wizard
	// $('#rootwizard').bootstrapWizard();
	//
	// // Mask
	// if ($('[data-mask]')
	// .length) {
	// $('[data-mask]')
	// .each(function () {
	//
	// $this = $(this);
	// var mask = $this.attr('data-mask') || 'error...',
	// mask_placeholder = $this.attr('data-mask-placeholder') || 'X';
	//
	// $this.mask(mask, {
	// placeholder: mask_placeholder
	// });
	// })

});
function changeDate(){
	$('#sikakuDate').val($('#datepicker').val());
}