/**
 * LoginBean.java  ログイン情報
 */
package bean;

import java.io.Serializable;

import common.StringCommon;

public class LoginBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String syainNo;// 社員番号
	private String password;// パスワード
	private String remember; // ログイン情報保存
	private boolean errFlg;// 入力チェックエラーフラグ
	private String errMsg;// エラーメッセージ

	/** コンストラクタ（ログイン情報の初期化） */
	public LoginBean() {
		setSyainNo("");
		setPassword("");
		setRemember("");
		setErrFlg(false);
		setErrMsg("");
	}

	public String getSyainNo() {
		return syainNo;
	}

	public void setSyainNo(String syainNo) {
		this.syainNo = StringCommon.cnvNull(syainNo);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = StringCommon.cnvNull(password);
	}

	public String getRemember() {
		return remember;
	}

	public void setRemember(String remember) {
		this.remember = StringCommon.cnvNull(remember);
	}

	public boolean isErrFlg() {
		return errFlg;
	}

	public void setErrFlg(boolean errFlg) {
		this.errFlg = errFlg;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		if (!errMsg.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsg = errMsg;
	}

}
