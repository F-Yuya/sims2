package bean;

import static parameter.SikakuParameters.*;

import java.io.Serializable;

import common.StringCommon;

public class SikakuBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String sikakucode;// 資格コード
	private String sikakuname;// 資格名
	private String sikakugroup; // 資格種別
	private String delflg;// 廃止フラグ
	private String status;// 廃止状態
	private boolean errFlg;// 入力チェックエラーフラグ
	private String errMsgSikakucode;// 資格コードエラーメッセージ
	private String errMsgSikakuname;// 資格名エラーメッセージ
	private String errMsgSikakugroup;// 資格区分エラーメッセージ
	private String errMsgStatus;// 廃止状況エラーメッセージ

	/** コンストラクタ（ログイン情報の初期化） */
	public SikakuBean() {
		setSikakucode("");
		setSikakuname("");
		setSikakugroup("");
		setStatus(NOT_DEL);
		//		setErrFlg(false);
		//		setErrMsgSikakucode("");
		//		setErrMsgSikakuname("");
		//		setErrMsgSikakugroup("");
		//		setErrMsgStatus("");
	}

	public String getSikakucode() {
		return sikakucode;
	}

	public void setSikakucode(String sikakucode) {
		this.sikakucode = sikakucode;
	}

	public String getSikakuname() {
		return sikakuname;
	}

	public void setSikakuname(String sikakuname) {
		this.sikakuname = sikakuname;
	}

	public String getSikakugroup() {
		return sikakugroup;
	}

	public void setSikakugroup(String sikakugroup) {
		this.sikakugroup = sikakugroup;
	}

	public String getDelflg() {
		return delflg;
	}

	public void setDelflg(String delFlg) {
		this.delflg = delFlg;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = StringCommon.cnvNull(status);
	}

	// 在籍状況からデータベースに設定するrdelflgに変換
	public String cnvdelFlg() {
		return NOT_DEL.equals(this.status) ? "0" : "1";
	}

	// delflgから在籍状況に変換
	public void cnvStatus(String delflg) {
		this.status = (StringCommon.cnvNull(delflg).equals("0")) ? NOT_DEL : DEL;
	}

	public boolean isErrFlg() {
		return errFlg;
	}

	public void setErrFlg(boolean errFlg) {
		this.errFlg = errFlg;
	}

	public String getErrMsgSikakucode() {
		return errMsgSikakucode;
	}

	public void setErrMsgSikakucode(String errMsgSikakucode) {
		if (!errMsgSikakucode.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgSikakucode = errMsgSikakucode;
	}

	public String getErrMsgSikakuname() {
		return errMsgSikakuname;
	}

	public void setErrMsgSikakuname(String errMsgSyainName) {
		if (!errMsgSyainName.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgSikakuname = errMsgSyainName;
	}

	public String getErrMsgSikakugroup() {
		return errMsgSikakugroup;
	}

	public void setErrMsgSikakugroup(String errMsgPassword) {
		if (!errMsgPassword.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgSikakugroup = errMsgPassword;
	}

	public String getErrMsgStatus() {
		return errMsgStatus;
	}

	public void setErrMsgStatus(String errMsgStatus) {
		if (!errMsgStatus.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgStatus = errMsgStatus;
	}
}

