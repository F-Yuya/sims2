package bean;

import java.io.Serializable;

public class SkillBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String syainno;// 社員番号
	private String sikakucode;// 資格コード
	private String sikakudate; // 資格取得日
	private boolean errFlg;// 入力チェックエラーフラグ
	private String errMsgSyainNo;// 社員番号エラーメッセージ
	private String errMsgSikakuCode;// 資格名エラーメッセージ
	private String errMsgSikakuDate;// 資格取得日エラーメッセージ

	/** コンストラクタ（ログイン情報の初期化） */
	public SkillBean() {
		setSyainno("");
		setSikakucode("");
		setSikakudate("");
		setErrMsgSyainNo("");
		setErrMsgSikakuCode("");
		setErrMsgSikakuDate("");
	}

	public String getSyainno() {
		return syainno;
	}

	public void setSyainno(String syainno) {
		this.syainno = syainno;
	}

	public String getSikakucode() {
		return sikakucode;
	}

	public void setSikakucode(String sikakucode) {
		this.sikakucode = sikakucode;
	}

	public String getSikakudate() {
		return sikakudate;
	}

	public void setSikakudate(String sikakudate) {
		this.sikakudate = sikakudate;
	}

	public boolean isErrFlg() {
		return errFlg;
	}

	public void setErrFlg(boolean errFlg) {
		this.errFlg = errFlg;
	}

	public String getErrMsgSyainNo() {
		return errMsgSyainNo;
	}

	public void setErrMsgSyainNo(String errMsgSyainNo) {
		if (!errMsgSyainNo.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgSyainNo = errMsgSyainNo;
	}

	public String getErrMsgSikakuCode() {
		return errMsgSikakuCode;
	}

	public void setErrMsgSikakuCode(String errMsgSikakuCode) {
		if (!errMsgSikakuCode.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgSikakuCode = errMsgSikakuCode;
	}

	public String getErrMsgSikakuDate() {
		return errMsgSikakuDate;
	}

	public void setErrMsgSikakuDate(String errMsgSikakuDate) {
		if (!errMsgSikakuDate.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgSikakuDate = errMsgSikakuDate;
	}
}
