/**
 * SyainBean.java  社員情報
 */
package bean;

import static parameter.SyainParameters.*;

import java.io.Serializable;

import common.StringCommon;

public class SyainBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String syainNo; // 社員番号
	private String syainName;// 社員名
	private String password;// パスワード
	private String status; // 在籍状況
	private boolean errFlg;// 入力チェックエラーフラグ
	private String errMsgSyainNo;// 社員番号エラーメッセージ
	private String errMsgSyainName;// 社員名エラーメッセージ
	private String errMsgPassword;// パスワードエラーメッセージ
	private String errMsgStatus;// 在籍状況エラーメッセージ

	/** コンストラクタ（社員情報の初期化） */
	public SyainBean() {
		setSyainNo("");
		setSyainName("");
		setPassword("");
		setStatus(NOT_RETIRE);
		setErrFlg(false);
		setErrMsgSyainNo("");
		setErrMsgSyainName("");
		setErrMsgPassword("");
		setErrMsgStatus("");
	}

	public String getSyainNo() {
		return syainNo;
	}

	public void setSyainNo(String syainNo) {
		this.syainNo = StringCommon.cnvNull(syainNo);
	}

	public String getSyainName() {
		return syainName;
	}

	public void setSyainName(String syainName) {
		this.syainName = StringCommon.cnvNull(syainName);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = StringCommon.cnvNull(password);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = StringCommon.cnvNull(status);
	}

	// 在籍状況からデータベースに設定するretireflgに変換
	public String cnvRetireFlg() {
		return NOT_RETIRE.equals(this.status) ? "0" : "1";
	}

	// retireflgから在籍状況に変換
	public void cnvStatus(String retireFlg) {
		this.status = (StringCommon.cnvNull(retireFlg).equals("0")) ? NOT_RETIRE : RETIRE;
	}

	public boolean isErrFlg() {
		return errFlg;
	}

	public void setErrFlg(boolean errFlg) {
		this.errFlg = errFlg;
	}

	public String getErrMsgSyainNo() {
		return errMsgSyainNo;
	}

	public void setErrMsgSyainNo(String errMsgSyainNo) {
		if (!errMsgSyainNo.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgSyainNo = errMsgSyainNo;
	}

	public String getErrMsgSyainName() {
		return errMsgSyainName;
	}

	public void setErrMsgSyainName(String errMsgSyainName) {
		if (!errMsgSyainName.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgSyainName = errMsgSyainName;
	}

	public String getErrMsgPassword() {
		return errMsgPassword;
	}

	public void setErrMsgPassword(String errMsgPassword) {
		if (!errMsgPassword.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgPassword = errMsgPassword;
	}

	public String getErrMsgStatus() {
		return errMsgStatus;
	}

	public void setErrMsgStatus(String errMsgStatus) {
		if (!errMsgStatus.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgStatus = errMsgStatus;
	}
}
