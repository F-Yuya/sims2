package bean;

import java.io.Serializable;

public class SyainSkillBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String syainno;// 社員番号
	private String syainName;// 社員名
	private String sikakucode;// 資格コード
	private String sikakuName; // 資格名
	private String sikakudate; // 資格取得日
	private boolean errFlg;// 入力チェックエラーフラグ
	private String errMsgSyainNo;// 社員番号エラーメッセージ
	private String errMsgSyainName;// 社員名エラーメッセージ

	public String getSyainno() {
		return syainno;
	}

	public void setSyainno(String syainno) {
		this.syainno = syainno;
	}

	public String getSyainName() {
		return syainName;
	}

	public void setSyainName(String syainName) {
		this.syainName = syainName;
	}

	public String getSikakucode() {
		return sikakucode;
	}

	public void setSikakucode(String sikakucode) {
		this.sikakucode = sikakucode;
	}

	public String getSikakuName() {
		return sikakuName;
	}

	public void setSikakuName(String sikakuname) {
		this.sikakuName = sikakuname;
	}

	public String getSikakudate() {
		return sikakudate;
	}

	public void setSikakudate(String sikakudate) {
		this.sikakudate = sikakudate;
	}

	public boolean isErrFlg() {
		return errFlg;
	}

	public void setErrFlg(boolean errFlg) {
		this.errFlg = errFlg;
	}

	public String getErrMsgSyainNo() {
		return errMsgSyainNo;
	}

	public void setErrMsgSyainNo(String errMsgSyainNo) {
		if (!errMsgSyainNo.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgSyainNo = errMsgSyainNo;
	}

	public String getErrMsgSyainName() {
		return errMsgSyainName;
	}

	public void setErrMsgSyainName(String errMsgSyainName) {
		if (!errMsgSyainName.isEmpty()) {
			this.errFlg = true;
		}
		this.errMsgSyainName = errMsgSyainName;
	}
}
