/**
 * StringCommon.java  文字列に関する操作をまとめたクラス
 */
package common;

public class StringCommon {
	/** 引数の文字列がnullの場合に""（長さ0の文字列）に変換
	 *
	 * @param str
	 * @return 文字列 */
	public static String cnvNull(String str) {
		if (str == null) {
			str = "";
		}
		return str;
	}
}
