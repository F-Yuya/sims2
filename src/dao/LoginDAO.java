package dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bean.LoginSession;
import exception.DatabaseException;
import exception.SystemException;

public class LoginDAO extends SIMSDAO {
	public LoginDAO() throws DatabaseException, SystemException {
		this.open();
	}

	public bean.LoginSession getLoginSession(bean.LoginBean loginBean) throws exception.DatabaseException, exception.SystemException {
		ResultSet rs;
		LoginSession loginSession = new LoginSession();
		String sql = "SELECT s.syainNo, s.syainName, s.password ,r.role "
				+ "FROM syain s,syain_roles r "
				+ "WHERE s.syainNo = r.syainNo AND s.syainNo = ? AND s.password = ? ";
		try (PreparedStatement pst = connection.prepareStatement(sql)) {
			pst.setString(1,loginBean.getSyainNo() );
			pst.setString(2, loginBean.getPassword());
			rs = pst.executeQuery();
			if (rs.next()) {
				loginSession.setSyainNo(rs.getString("syainNo"));
				loginSession.setSyainName(rs.getString("syainName"));
				loginSession.setPassword(rs.getString("password"));
				loginSession.setRole(rs.getString("role"));
				loginSession.setLoginFlg(true);
			}
		} catch (SQLException e) {
			throw new DatabaseException("", e);
		} finally {
			this.close();
		}
		return loginSession;

	}
}
