package dao;

import static parameter.ExceptionMessages.*;
import static parameter.RoleNames.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.SikakuBean;
import exception.DatabaseException;
import exception.SystemException;

public class SikakuDAO extends SIMSDAO {

	/**
	 * コンストラクタ
	 *
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public SikakuDAO() throws DatabaseException, SystemException {
		this.open();
	}

	/**
	 * 資格テーブルを検索し、全資格情報を取得する。
	 *
	 * @param role
	 * @return 全資格情報
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public ArrayList<SikakuBean> getSikakuBeanList(String role) throws DatabaseException, SystemException {
		ArrayList<SikakuBean> sikakuBeanList = new ArrayList<SikakuBean>();

		try (Statement statement = connection.createStatement();) {
			String sql;
			if (USER.equals(role) || MANAGER.equals(role)) {
				sql = "SELECT * FROM sikaku "
						+ "WHERE delflg = '0'"
						+ "ORDER BY sikakucode";
			} else {
				sql = "SELECT * FROM sikaku "
						+ "ORDER BY sikakucode";
			}
			ResultSet resultSet = statement.executeQuery(sql);
			if (!ADMIN.equals(role)) {
				while (resultSet.next()) {
					SikakuBean sikakuBean = new SikakuBean();
					sikakuBean.setSikakucode(resultSet.getString(1));
					sikakuBean.setSikakuname(resultSet.getString(2));
					sikakuBean.setSikakugroup(resultSet.getString(3));
					sikakuBeanList.add(sikakuBean);
				}
			} else {
				while (resultSet.next()) {
					SikakuBean sikakuBean = new SikakuBean();
					sikakuBean.setSikakucode(resultSet.getString(1));
					sikakuBean.setSikakuname(resultSet.getString(2));
					sikakuBean.setSikakugroup(resultSet.getString(3));
					sikakuBean.cnvStatus(resultSet.getString(4));
					sikakuBeanList.add(sikakuBean);
				}
			}
		} catch (SQLException e) {
			throw new DatabaseException(SYAIN_SELECT_EXCEPTION, e);
		} finally {
			this.close();
		}
		return sikakuBeanList;
	}

	/**
	 * 資格テーブルを検索し、登録済みの資格かをチェックする。
	 *
	 * @param sikakucode
	 * @return 登録済みの場合ture
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public boolean isEntryed(String sikakucode) throws DatabaseException, SystemException {
		String sql = "SELECT sikakucode FROM sikaku WHERE sikakucode = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, sikakucode);
			ResultSet resultSet = preparedStatement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DatabaseException(SIKAKU_SELECT_EXCEPTION, e);
		} finally {
			this.close();
		}
	}

	/**
	 * 資格情報を登録する。
	 *
	 * @param sikakuBean
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public void entrySikaku(SikakuBean sikakuBean) throws DatabaseException, SystemException {
		String sql = "INSERT INTO sikaku VALUES(? ,?, ?, '0')";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, sikakuBean.getSikakucode());
			preparedStatement.setString(2, sikakuBean.getSikakuname());
			preparedStatement.setString(3, sikakuBean.getSikakugroup());
			int cnt = preparedStatement.executeUpdate();
			System.out.println("資格情報" + cnt + "件追加");
			if (cnt == 1) {
				this.commit(connection);
			} else {
				this.rollback(connection);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SIKAKU_ENTRY_EXCEPTION, e);
		} finally {
			this.close();
		}
	}

	/**
	 * 資格コードで資格テーブルを検索し、資格情報を取得する。
	 *
	 * @return 資格情報
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public SikakuBean getSikakuBean(String sikakucode) throws DatabaseException, SystemException {
		SikakuBean sikakuBean = new SikakuBean();
		String sql = "SELECT sikakucode, sikakuname, sikakugroup, delflg FROM sikaku WHERE sikakucode = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, sikakucode);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				sikakuBean.setSikakucode(resultSet.getString(1));
				sikakuBean.setSikakuname(resultSet.getString(2));
				sikakuBean.setSikakugroup(resultSet.getString(3));
				sikakuBean.cnvStatus(resultSet.getString(4));
			}
		} catch (SQLException e) {
			throw new DatabaseException(SIKAKU_SELECT_EXCEPTION, e);
		} finally {
			this.close();
		}
		return sikakuBean;
	}

	/**
	 * 資格情報を更新する。
	 *
	 * @param updateSikakuBean
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public void updateSikaku(SikakuBean updateSikakuBean) throws DatabaseException, SystemException {
		String sql = "UPDATE sikaku SET sikakuname = ?, sikakugroup = ?, delflg = ? WHERE sikakucode = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, updateSikakuBean.getSikakuname());
			preparedStatement.setString(2, updateSikakuBean.getSikakugroup());
			preparedStatement.setString(3, updateSikakuBean.cnvdelFlg());
			preparedStatement.setString(4, updateSikakuBean.getSikakucode());
			int cnt = preparedStatement.executeUpdate();
			System.out.println("資格情報" + cnt + "件更新");
			if (cnt == 1) {
				this.commit(connection);
			} else {
				this.rollback(connection);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SIKAKU_UPDATE_EXCEPTION, e);
		} finally {
			this.close();
		}
	}
}
