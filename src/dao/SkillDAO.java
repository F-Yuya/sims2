package dao;

import static parameter.ExceptionMessages.*;
import static parameter.RoleNames.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import bean.SyainSkillBean;
import exception.DatabaseException;
import exception.SystemException;

public class SkillDAO extends SIMSDAO {

	/**
	 * コンストラクタ
	 *
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public SkillDAO() throws DatabaseException, SystemException {
		this.open();
	}

	/**
	 * スキルテーブルを検索し、スキル情報を取得する。
	 *
	 * @param role , syainNo
	 * @return スキル情報
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public ArrayList<SyainSkillBean> getSyainSkillBeanList(String role, String syainNo) throws DatabaseException, SystemException {
		ArrayList<SyainSkillBean> syainSkillBeanList = new ArrayList<SyainSkillBean>();
			String sql;
			if (USER.equals(role)) {
				sql = "SELECT syain.syainno, syain.syainname, sikaku.sikakucode, sikaku.sikakuname, skill.sikakudate FROM syain, skill, sikaku "
					+ "WHERE syain.syainno = skill.syainno AND skill.sikakucode = sikaku.sikakucode AND skill.syainno = ? "
						+ "ORDER BY sikaku.sikakucode ";
			} else {
				sql = "SELECT syain.syainno, syain.syainname, sikaku.sikakucode, sikaku.sikakuname, skill.sikakudate FROM syain, skill, sikaku "
						+ "WHERE syain.syainno = skill.syainno AND skill.sikakucode = sikaku.sikakucode  "
					+ "ORDER BY syain.syainno, sikaku.sikakucode ";
			}
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			if (USER.equals(role))
				preparedStatement.setString(1, syainNo);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				SyainSkillBean syainSkillBean = new SyainSkillBean();
				syainSkillBean.setSyainno(resultSet.getString(1));
				syainSkillBean.setSyainName(resultSet.getString(2));
				syainSkillBean.setSikakucode(resultSet.getString(3));
				syainSkillBean.setSikakuName(resultSet.getString(4));
				syainSkillBean.setSikakudate(resultSet.getString(5));
				syainSkillBeanList.add(syainSkillBean);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SYAIN_SELECT_EXCEPTION, e);
		} finally {
			this.close();
		}
		return syainSkillBeanList;
	}

	/**
	 * スキルテーブルを検索し、登録済みのスキルかをチェックする。
	 *
	 * @param syainNo, sikakucode
	 * @return 登録済みの場合ture
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public boolean isEntryed(String syainNo, String sikakucode) throws DatabaseException, SystemException {
		String sql = "SELECT syainno FROM skill WHERE syainno = ? AND sikakucode = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, syainNo);
			preparedStatement.setString(2, sikakucode);
			ResultSet resultSet = preparedStatement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DatabaseException(SKILL_SELECT_EXCEPTION, e);
		} finally {
			this.close();
		}
	}

	/**
	 * スキル情報を登録する。
	 *
	 * @param skillBean
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public void entrySkill(SyainSkillBean syainSkillBean) throws DatabaseException, SystemException {

		String sql = "INSERT INTO skill VALUES(? ,?, TO_DATE(?,'YYYY/MM/DD')) ";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, syainSkillBean.getSyainno());
			preparedStatement.setString(2, syainSkillBean.getSikakucode());
			preparedStatement.setString(3, syainSkillBean.getSikakudate());
			int cnt = preparedStatement.executeUpdate();
			System.out.println("スキル情報" + cnt + "件追加");
			if (cnt == 1) {
				this.commit(connection);
			} else {
				this.rollback(connection);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SKILL_ENTRY_EXCEPTION, e);
		} finally {
			this.close();
		}
	}

	public SyainSkillBean getSyainSkillBean(String syainNo, String sikakucode) throws DatabaseException {
		SyainSkillBean syainSkillBean = new SyainSkillBean();
		String sql = "SELECT syain.syainno, syain.syainname, sikaku.sikakucode, sikaku.sikakuname, skill.sikakudate FROM syain, skill, sikaku "
				+ "WHERE syain.syainno = skill.syainno AND skill.sikakucode = sikaku.sikakucode AND skill.syainno = ? AND skill.sikakucode = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, syainNo);
			preparedStatement.setString(2, sikakucode);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				syainSkillBean.setSyainno(resultSet.getString("syainno"));
				syainSkillBean.setSyainName(resultSet.getString("syainname"));
				syainSkillBean.setSikakucode(resultSet.getString("sikakucode"));
				syainSkillBean.setSikakuName(resultSet.getString("sikakuname"));
				syainSkillBean.setSikakudate(resultSet.getString("sikakudate"));
			}
		} catch (SQLException e) {
			throw new DatabaseException(SKILL_SELECT_EXCEPTION, e);
		} finally {
			this.close();
		}
		return syainSkillBean;
	}

	/**
	 * スキル情報を更新する。
	 *
	 * @param updateSyainSkillBean
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public void updateSkill(SyainSkillBean updateSyainSkillBean) throws DatabaseException, SystemException {
		String sql = "UPDATE skill SET sikakudate = TO_DATE(?,'YYYY/MM/DD') WHERE syainno = ? AND sikakucode = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, updateSyainSkillBean.getSikakudate());
			preparedStatement.setString(2, updateSyainSkillBean.getSyainno());
			preparedStatement.setString(3, updateSyainSkillBean.getSikakucode());
			int cnt = preparedStatement.executeUpdate();
			System.out.println("スキル情報" + cnt + "件更新");
			if (cnt == 1) {
				this.commit(connection);
			} else {
				this.rollback(connection);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SKILL_UPDATE_EXCEPTION, e);
		} finally {
			this.close();
		}
	}

	/**
	 * スキル情報を削除する。
	 *
	 * @param updateSyainSkillBean
	 * @throws DatabaseException
	 * @throws SystemException
	 */
	public void deleteSkill(SyainSkillBean updateSyainSkillBean) throws DatabaseException, SystemException {
		String sql = "DELETE FROM skill WHERE syainno = ? AND sikakucode = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, updateSyainSkillBean.getSyainno());
			preparedStatement.setString(2, updateSyainSkillBean.getSikakucode());
			int cnt = preparedStatement.executeUpdate();
			System.out.println("スキル情報" + cnt + "件削除");
			if (cnt == 1) {
				this.commit(connection);
			} else {
				this.rollback(connection);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SKILL_UPDATE_EXCEPTION, e);
		} finally {
			this.close();
		}
	}
}
