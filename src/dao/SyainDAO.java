/**
 * SyainDAO.java 社員テーブルに対する処理をまとめたクラス
 */
package dao;

import static parameter.ExceptionMessages.*;
import static parameter.RoleNames.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.SyainBean;
import exception.DatabaseException;
import exception.SystemException;

public class SyainDAO extends SIMSDAO {

	/** コンストラクタ
	 *
	 * @throws DatabaseException
	 * @throws SystemException */
	public SyainDAO() throws DatabaseException, SystemException {
		this.open();
	}

	/** 社員テーブルを検索し、全社員情報を取得する。
	 *
	 * @param role
	 * @return 全社員情報
	 * @throws DatabaseException
	 * @throws SystemException */
	public ArrayList<SyainBean> getSyainBeanList(String role) throws DatabaseException, SystemException {
		ArrayList<SyainBean> syainBeanList = new ArrayList<SyainBean>();
		try (Statement statement = connection.createStatement();) {
			String sql;
			if (USER.equals(role)) {
				sql = "SELECT syain.syainno, syain.syainname FROM syain, syain_roles "
						+ "WHERE syain.syainno = syain_roles.syainno AND syain.retireflg = '0' AND syain_roles.role = 'user' "
						+ "ORDER BY syain.syainno DESC, syain.syainname";
			} else if (MANAGER.equals(role)) {
				sql = "SELECT syain.syainno, syain.syainname FROM syain, syain_roles "
						+ "WHERE syain.syainno = syain_roles.syainno AND syain.retireflg = '0' AND syain_roles.role <> 'admin' "
						+ "ORDER BY syain.syainno DESC, syain.syainname";
			} else {
				sql = "SELECT syainno, syainname, password, retireflg FROM syain "
						+ "ORDER BY retireflg, syainno DESC, syainname";
			}
			ResultSet resultSet = statement.executeQuery(sql);
			if (!ADMIN.equals(role)) {
				while (resultSet.next()) {
					SyainBean syainBean = new SyainBean();
					syainBean.setSyainNo(resultSet.getString(1));
					syainBean.setSyainName(resultSet.getString(2));
					syainBean.setPassword("*****");
					syainBeanList.add(syainBean);
				}
			} else {
				while (resultSet.next()) {
					SyainBean syainBean = new SyainBean();
					syainBean.setSyainNo(resultSet.getString(1));
					syainBean.setSyainName(resultSet.getString(2));
					syainBean.setPassword(resultSet.getString(3));
					syainBean.cnvStatus(resultSet.getString(4));
					syainBeanList.add(syainBean);
				}
			}
		} catch (SQLException e) {
			throw new DatabaseException(SYAIN_SELECT_EXCEPTION, e);
		} finally {
			this.close();
		}
		return syainBeanList;
	}

	/** 社員テーブルを検索し、登録済みの社員かをチェックする。
	 *
	 * @param syainNo
	 * @return 登録済みの場合ture
	 * @throws DatabaseException
	 * @throws SystemException */
	public boolean isEntryed(String syainNo) throws DatabaseException, SystemException {
		String sql = "SELECT syainno FROM syain WHERE syainno = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, syainNo);
			ResultSet resultSet = preparedStatement.executeQuery();
			return resultSet.next();
		} catch (SQLException e) {
			throw new DatabaseException(SYAIN_SELECT_EXCEPTION, e);
		} finally {
			this.close();
		}
	}

	/** 社員情報を登録する。
	 *
	 * @param syainBean
	 * @throws DatabaseException
	 * @throws SystemException */
	public void entrySyain(SyainBean syainBean) throws DatabaseException, SystemException {
		String sql = "INSERT INTO syain VALUES(? ,?, ?, '0')";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, syainBean.getSyainNo());
			preparedStatement.setString(2, syainBean.getSyainName());
			preparedStatement.setString(3, syainBean.getPassword());
			int cnt = preparedStatement.executeUpdate();
			System.out.println("社員情報" + cnt + "件追加");
			if (cnt == 1) {
				this.commit(connection);
			} else {
				this.rollback(connection);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SYAIN_ENTRY_EXCEPTION, e);
		} finally {
			this.close();
		}
	}

	/** 社員番号で社員テーブルを検索し、社員情報を取得する。
	 *
	 * @return 社員情報
	 * @throws DatabaseException
	 * @throws SystemException */
	public SyainBean getSyainBean(String syainNo) throws DatabaseException, SystemException {
		SyainBean syainBean = new SyainBean();
		String sql = "SELECT syainno, syainname, password, retireflg FROM syain WHERE syainno = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, syainNo);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				syainBean.setSyainNo(resultSet.getString(1));
				syainBean.setSyainName(resultSet.getString(2));
				syainBean.setPassword(resultSet.getString(3));
				syainBean.cnvStatus(resultSet.getString(4));
			}
		} catch (SQLException e) {
			throw new DatabaseException(SYAIN_SELECT_EXCEPTION, e);
		} finally {
			this.close();
		}
		return syainBean;
	}

	/** 社員情報を更新する。
	 *
	 * @param updateSyainBean
	 * @throws DatabaseException
	 * @throws SystemException */
	public void updateSyain(SyainBean updateSyainBean) throws DatabaseException, SystemException {
		String sql = "UPDATE syain SET syainname = ?, password = ?, retireflg = ? WHERE syainno = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, updateSyainBean.getSyainName());
			preparedStatement.setString(2, updateSyainBean.getPassword());
			preparedStatement.setString(3, updateSyainBean.cnvRetireFlg());
			preparedStatement.setString(4, updateSyainBean.getSyainNo());
			int cnt = preparedStatement.executeUpdate();
			System.out.println("社員情報" + cnt + "件更新");
			if (cnt == 1) {
				this.commit(connection);
			} else {
				this.rollback(connection);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SYAIN_UPDATE_EXCEPTION, e);
		} finally {
			this.close();
		}
	}
}
