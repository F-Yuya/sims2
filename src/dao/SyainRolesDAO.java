/**
 * SyainRolesDAO.java 社員ロールテーブルに対する処理をまとめたクラス
 */
package dao;

import static parameter.ExceptionMessages.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import bean.LoginSession;
import bean.SyainBean;
import exception.DatabaseException;
import exception.SystemException;

public class SyainRolesDAO extends SIMSDAO {

	/** コンストラクタ
	 *
	 * @throws DatabaseException
	 * @throws SystemException */
	public SyainRolesDAO() throws DatabaseException, SystemException {
		this.open();
	}

	/** ロール情報を登録する。
	 *
	 * @param syainNo
	 *
	 * @throws DatabaseException
	 * @throws SystemException */
	public void entrySyainRole(String syainNo) throws DatabaseException, SystemException {
		String sql = "INSERT INTO syain_roles VALUES(?, 'user')";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, syainNo);
			int cnt = preparedStatement.executeUpdate();
			System.out.println("社員ロール情報" + cnt + "件追加");
			if (cnt == 1) {
				this.commit(connection);
			} else {
				this.rollback(connection);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SYAIN_ROLES_ENTRY_EXCEPTION, e);
		} finally {
			this.close();
		}
	}

	/** 社員テーブルを検索し、全社員情報を取得する。
	 *
	 * @param role
	 * @return 全社員ロール情報
	 * @throws DatabaseException
	 * @throws SystemException */
	public ArrayList<LoginSession> getLoginSessionList() throws DatabaseException, SystemException {
		ArrayList<LoginSession> loginSessionList = new ArrayList<LoginSession>();
		try (Statement statement = connection.createStatement();) {
			String sql;
			sql = "SELECT syain.syainno, syain.syainname, syain_roles.role FROM syain, syain_roles "
					+ "WHERE syain.syainno = syain_roles.syainno "
					+ "ORDER BY syain.syainno, syain.syainname";
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				LoginSession loginSession = new LoginSession();
				loginSession.setSyainNo(resultSet.getString(1));
				loginSession.setSyainName(resultSet.getString(2));
				loginSession.setPassword("*****");
				loginSession.setRole(resultSet.getString(3));
				loginSessionList.add(loginSession);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SYAIN_SELECT_EXCEPTION, e);
		} finally {
			this.close();
		}
		return loginSessionList;
	}

	/** ロール情報を更新する。
	 *
	 * @param updateSyainBean
	 * @throws DatabaseException
	 * @throws SystemException */
	public void updateSyain(SyainBean updateSyainBean) throws DatabaseException, SystemException {
		String sql = "UPDATE syain_roles SET ROLE = ? WHERE syainno = ?";
		try (PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
			preparedStatement.setString(1, updateSyainBean.getSyainName());
			preparedStatement.setString(2, updateSyainBean.getSyainNo());
			int cnt = preparedStatement.executeUpdate();
			System.out.println("ロール情報" + cnt + "件更新");
			if (cnt == 1) {
				this.commit(connection);
			} else {
				this.rollback(connection);
			}
		} catch (SQLException e) {
			throw new DatabaseException(SYAIN_UPDATE_EXCEPTION, e);
		} finally {
			this.close();
		}
	}
}
