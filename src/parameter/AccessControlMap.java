/**
 * AccessControlMap.java  アクセス制御情報
 */
package parameter;

import static parameter.RoleNames.*;
import static parameter.ServletPaths.*;

import java.util.HashMap;
import java.util.Map;

public class AccessControlMap {

	private AccessControlMap() {
	}

	/** 全ロールがアクセス可能なマップ。ロール名をキーにtrue（アクセス許可）を設定。 */
	private static final Map<String, Boolean> ALL_ACCESS = new HashMap<String, Boolean>() {
		{
			put(ADMIN, true);
			put(MANAGER, true);
			put(USER, true);
		}
	};

	/** 部長以上がアクセス可能なマップ。ロール名をキーにtrue（アクセス許可）、またはfalse（アクセス拒否）を設定。 */
	private static final Map<String, Boolean> MORE_MANAGER_ACCESS = new HashMap<String, Boolean>() {
		{
			put(ADMIN, true);
			put(MANAGER, true);
			put(USER, false);
		}
	};

	/** 管理者のみがアクセス可能なマップ。ロール名をキーにtrue（アクセス許可）、またはfalse（アクセス拒否）を設定。 */
	private static final Map<String, Boolean> ONLY_ADMIN_ACCESS = new HashMap<String, Boolean>() {
		{
			put(ADMIN, true);
			put(MANAGER, false);
			put(USER, false);
		}
	};

	/** アクセス制御のマップ。サーブレットパスをキーにどのロールがアクセス可能なマップかを設定。 */
	public static final Map<String, Map<String, Boolean>> ACCESS_CONTROL_MAP = new HashMap<String, Map<String, Boolean>>() {
		{
			put(TOP, ALL_ACCESS);
			put(SYAIN_LIST, ALL_ACCESS);
			put(SYAIN_ENTRY, ONLY_ADMIN_ACCESS);
			put(SYAIN_UPDATE, MORE_MANAGER_ACCESS);
			put(SAMPLE_ENTRY, MORE_MANAGER_ACCESS);
			put(SIKAKU_LIST, ALL_ACCESS);
			put(SIKAKU_ENTRY, ONLY_ADMIN_ACCESS);
			put(SIKAKU_UPDATE, MORE_MANAGER_ACCESS);
			put(SKILL_LIST, ALL_ACCESS);
			put(SKILL_ENTRY, MORE_MANAGER_ACCESS);
			put(SKILL_UPDATE, MORE_MANAGER_ACCESS);
			put(ROLE_LIST, ONLY_ADMIN_ACCESS);
			put(ROLE_UPDATE, ONLY_ADMIN_ACCESS);
		}
	};
}
