/**
 * ButtonNames.java  ボタンのvalue値
 */
package parameter;

public class ButtonNames {
	private ButtonNames() {
	}

	public static final String LOGIN_BTN = "Login";
	public static final String SUBMIT_BTN = "Submit";
	public static final String ENTRY_BTN = "Entry";
	public static final String BACK_BTN = "Back";
	public static final String UPDATE_BTN = "Update";
	public static final String DELETE_BTN = "Delete";
}
