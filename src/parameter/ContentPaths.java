/** ContentPaths.java  コンテンツパス情報*/
package parameter;

public class ContentPaths {
	private ContentPaths() {
	}

	public static final String LOGIN_JSP = "/WEB-INF/view/login/login.jsp";
	public static final String LOGIN_ERROR_JSP = "/WEB-INF/view/login/error.jsp";

	public static final String TOP_JSP = "/WEB-INF/view/top.jsp";
	public static final String ERROR_JSP = "/WEB-INF/view/error.jsp";

	public static final String SYAIN_TOP_JSP = "/WEB-INF/view/syain/top.jsp";
	public static final String SYAIN_LIST_JSP = "/WEB-INF/view/syain/list.jsp";
	public static final String SYAIN_ENTRY_INPUT_JSP = "/WEB-INF/view/syain/entryinput.jsp";
	public static final String SYAIN_ENTRY_CONFIRM_JSP = "/WEB-INF/view/syain/entryconfirm.jsp";
	public static final String SYAIN_ENTRY_COMP_JSP = "/WEB-INF/view/syain/entrycomp.jsp";
	public static final String SYAIN_UPDATE_INPUT_JSP = "/WEB-INF/view/syain/updateinput.jsp";
	public static final String SYAIN_UPDATE_COMP_JSP = "/WEB-INF/view/syain/updatecomp.jsp";

	public static final String SIKAU_TOP_JSP = "/WEB-INF/view/sikaku/top.jsp";
	public static final String SIKAKU_LIST_JSP = "/WEB-INF/view/sikaku/list.jsp";
	public static final String SIKAKU_ENTRY_INPUT_JSP = "/WEB-INF/view/sikaku/entryinput.jsp";
	public static final String SIKAKU_ENTRY_CONFIRM_JSP = "/WEB-INF/view/sikaku/entryconfirm.jsp";
	public static final String SIKAKU_ENTRY_COMP_JSP = "/WEB-INF/view/sikaku/entrycomp.jsp";
	public static final String SIKAKU_UPDATE_INPUT_JSP = "/WEB-INF/view/sikaku/updateinput.jsp";
	public static final String SIKAKU_UPDATE_COMP_JSP = "/WEB-INF/view/sikaku/updatecomp.jsp";

	public static final String SKILL_TOP_JSP = "/WEB-INF/view/skill/top.jsp";
	public static final String SKILL_LIST_JSP = "/WEB-INF/view/skill/list.jsp";
	public static final String SKILL_ENTRY_INPUT_JSP = "/WEB-INF/view/skill/entryinput.jsp";
	public static final String SKILL_ENTRY_CONFIRM_JSP = "/WEB-INF/view/skill/entryconfirm.jsp";
	public static final String SKILL_ENTRY_COMP_JSP = "/WEB-INF/view/skill/entrycomp.jsp";
	public static final String SKILL_UPDATE_INPUT_JSP = "/WEB-INF/view/skill/updateinput.jsp";
	public static final String SKILL_UPDATE_COMP_JSP = "/WEB-INF/view/skill/updatecomp.jsp";

	public static final String ROLE_TOP_JSP = "/WEB-INF/view/Role/top.jsp";
	public static final String ROLE_LIST_JSP = "/WEB-INF/view/Role/list.jsp";
	public static final String ROLE_UPDATE_INPUT_JSP = "/WEB-INF/view/Role/updateinput.jsp";
	public static final String ROLE_UPDATE_COMP_JSP = "/WEB-INF/view/Role/updatecomp.jsp";

	public static final String SAMPLE_ENTRY_INPUT_JSP = "/WEB-INF/view/sample/entryinput.jsp";
	public static final String SAMPLE_ENTRY_CONFIRM_JSP = "/WEB-INF/view/sample/entryconfirm.jsp";
}
