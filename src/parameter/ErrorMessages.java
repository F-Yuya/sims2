/**
 * ErrorMessages.java  エラーメッセージの定義クラス
 */
package parameter;

public class ErrorMessages {
	private ErrorMessages() {
	}

	public static final String SESSION_TIMEOUT = "ログイン情報の取得に失敗しました。<br />再度ログインしてください。";
	public static final String ACCESS_ERROR = "アクセスエラーです。<br />使用を許可された社員でログインし直してください。";
	public static final String SYAINNO_REQUIRED = "社員番号を入力してください。";
	public static final String SYAINNO_DIGIT = "社員番号は半角英数5文字で入力してください。";
	public static final String SYAINNO_ISENTRYED = "この社員番号は登録済みです。もう一度入力し直してください。";
	public static final String NOTEXISTS_SYAIN = "存在しない社員、または退社した社員の情報は変更できません。";
	public static final String SYAINNAME_REQUIRED = "社員名を入力してください。";
	public static final String SYAINNAME_DIGIT = "社員名は全角25文字以内で入力してください。";
	public static final String PASSWORD_REQUIRED = "パスワードを入力してください。";
	public static final String PASSWORD_DIGIT = "パスワードは半角英数4～10文字以内で入力してください。";
	public static final String STATUS_REQUIRED = "在籍状況を選択してください。";
	public static final String LOGIN_PARAM_MISTAKE = "社員番号、またはパスワードに誤りがあります。<br />入力項目を確認し、再度ログインしてください。";

	public static final String SIKAKUCODE_REQUIRED = "資格コードを入力してください。";
	public static final String SIKAKUCODE_DIGIT = "資格コードは半角数字3文字で入力してください。";
	public static final String SIKAKUCODE_ISENTRYED = "この資格コードは登録済みです。もう一度入力し直してください。";
	public static final String NOTEXISTS_SIKAKU = "存在しない資格、または廃止した資格の情報は変更できません。";
	public static final String SIKAKUNAME_REQUIRED = "資格名を入力してください。";
	public static final String SIKAKUNAME_DIGIT = "資格名は全角2０文字以内で入力してください。";
	public static final String REQUIRED = "資格区分を入力してください。";
	public static final String SIKAKUGROUP_DIGIT = "資格区分は半角英数40文字以内で入力してください。";
	public static final String SIKAKU_STATUS_REQUIRED = "廃止状況を選択してください。";
	public static final String SIKAKUGROUP_REQUIRED = "資格種別を選択してください。";


	public static final String SKILL_ISENTRYED = "このスキル情報は登録済みです。もう一度入力し直してください。";
	public static final String NOTEXISTS_SKILL = "存在しないスキルの情報の情報は変更できません。";

	public static final String GETSYAINNO_REQUIRED = "社員を選択してください。";
	public static final String GETSIKAKUCODE_REQUIRED = "取得資格を選択してください。";
	public static final String GETSIKAKUDATE_REQUIRED = "資格取得日を選択してください。";
	public static final String GETSIKAKUDATE_MISTAKE = "資格取得日を正しく選択してください。";
}
