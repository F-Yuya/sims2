/** ServletPaths.java  サーブレットパス情報*/
package parameter;

public class ServletPaths {
	private ServletPaths() {
	}

	public static final String LOGIN = "/Login";// ログイン画面
	public static final String LOGIN_URL = "Login";// ログイン画面URL

	public static final String TOP = "/Top";// トップ画面
	public static final String TOP_URL = "Top";// トップ画面URL

	public static final String SYAIN_LIST = "/SyainList";// 社員情報一覧
	public static final String SYAIN_ENTRY = "/SyainEntry";// 社員情報登録
	public static final String SYAIN_UPDATE = "/SyainUpdate"; // 社員情報更新

	public static final String SIKAKU_LIST = "/SikakuList";// 資格情報一覧
	public static final String SIKAKU_ENTRY = "/SikakuEntry";// 資格情報登録
	public static final String SIKAKU_UPDATE = "/SikakuUpdate"; // 資格情報更新

	public static final String SKILL_LIST = "/SkillList";// スキル情報一覧
	public static final String SKILL_ENTRY = "/SkillEntry";// スキル情報登録
	public static final String SKILL_UPDATE = "/SkillUpdate"; // スキル情報更新

	public static final String ROLE_LIST = "/RoleList";// 社員ロール一覧
	public static final String ROLE_UPDATE = "/RoleUpdate";// 社員ロール更新

	public static final String SAMPLE_ENTRY = "/SampleEntry";// サンプル情報登録
}
