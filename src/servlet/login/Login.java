package servlet.login;

import static parameter.ContentPaths.*;
import static parameter.ServletPaths.*;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.LoginBean;
import bean.LoginSession;
import dao.LoginDAO;
import dao.SIMSDAO;
import exception.DatabaseException;
import exception.SystemException;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try {
			new SIMSDAO().init();
		} catch (SystemException | DatabaseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletContext().getRequestDispatcher(LOGIN_JSP).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("button").equals("Clear")) {
			request.setAttribute("loginSession", new LoginSession());
			getServletContext().getRequestDispatcher(LOGIN_JSP).forward(request, response);
			return;
		}
		LoginBean loginBean = new LoginBean();
		LoginSession loginSession = new LoginSession();
		loginBean.setSyainNo(request.getParameter("syainNo"));
		loginBean.setPassword(request.getParameter("password"));
		try {
			LoginDAO logindao = new LoginDAO();
			loginSession = logindao.getLoginSession(loginBean);
			if (loginSession.isLoginFlg()) {
				HttpSession session = request.getSession();
				session.setAttribute("loginSession", loginSession);
				response.sendRedirect(TOP_URL);
			} else {
				loginBean.setErrFlg(true);
				request.setAttribute("loginBean", loginBean);
				getServletContext().getRequestDispatcher(LOGIN_JSP).forward(request, response);
			}
		} catch (DatabaseException | SystemException e1) {
			getServletContext().getRequestDispatcher(LOGIN_ERROR_JSP).forward(request, response);
		}
	}

}
