package servlet.role;

import static parameter.ContentPaths.*;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.LoginSession;
import dao.SyainRolesDAO;
import exception.DatabaseException;
import exception.SystemException;
import servlet.SIMSServlet;

@WebServlet("/RoleList")
public class RoleList extends SIMSServlet {
	private static final long serialVersionUID = 1L;

	/** 社員情報一覧画面を表示する。（GETメソッド） */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 社員テーブルを検索して社員情報を取得
		ArrayList<LoginSession> loginSessionList;
		try {
			SyainRolesDAO syainRolesDAO = new SyainRolesDAO();
			loginSessionList = syainRolesDAO.getLoginSessionList();
		} catch (SystemException | DatabaseException e) {
			// 例外が発生した場合、エラー画面に遷移
			e.printStackTrace();
			request.setAttribute("exMsg", e.getMessage());
			getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
			return;
		}
		request.setAttribute("loginSessionList", loginSessionList);
		getServletContext().getRequestDispatcher(ROLE_LIST_JSP).forward(request, response);
	}

}
