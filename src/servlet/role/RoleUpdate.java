package servlet.role;

import static parameter.ButtonNames.*;
import static parameter.ContentPaths.*;
import static parameter.ErrorMessages.*;
import static parameter.RoleNames.*;
import static parameter.SyainParameters.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.SyainBean;
import common.StringCommon;
import dao.SyainDAO;
import exception.DatabaseException;
import exception.SystemException;
import servlet.SIMSServlet;
import validator.SyainValidators;

@WebServlet("/RoleUpdate")
public class RoleUpdate extends SIMSServlet {
	private static final long serialVersionUID = 1L;

	/** ロール情報更新入力画面を表示する。（GETメソッド） */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String syainNo = request.getParameter("syainNo");
		SyainBean syainBean;
		try {
			SyainDAO syainDAO = new SyainDAO();
			syainBean = syainDAO.getSyainBean(syainNo);

			// 存在しない社員または退社した社員の場合、エラー画面に遷移
			if (StringCommon.cnvNull(syainBean.getSyainNo()).equals("") || RETIRE.equals(syainBean.getStatus())) {
				request.setAttribute("errMsg", NOTEXISTS_SYAIN);
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
		} catch (SystemException | DatabaseException e) {
			// 例外が発生した場合、エラー画面に遷移
			e.printStackTrace();
			request.setAttribute("exMsg", e.getMessage());
			getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
			return;
		}
		// 更新前の社員情報をセッションとリクエストに設定
		HttpSession session = request.getSession(false);
		session.setAttribute("syainBean", syainBean);
		request.setAttribute("updateSyainBean", syainBean);
		getServletContext().getRequestDispatcher(SYAIN_UPDATE_INPUT_JSP).forward(request, response);
	}

	/** ロール情報更新機能でボタンが押された場合の処理。（POSTメソッド） */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッションから更新前の社員情報を取得
		HttpSession session = request.getSession(false);
		SyainBean syainBean = (SyainBean) session.getAttribute("syainBean");
		// ロールを取得
		String role = (String) request.getAttribute("role");
		// リクエストパラメータから更新する社員情報を取得
		String syainNo = request.getParameter("syainNo");
		String syainName = request.getParameter("syainName");
		String password = request.getParameter("password");
		String status = request.getParameter("status");
		String button = request.getParameter("button");

		// 部長の場合、パスワードと在籍状況はセッション（更新前）の社員情報から取得
		if (MANAGER.equals(role)) {
			password = syainBean.getPassword();
		}
		// 社員情報を生成
		SyainBean updateSyainBean = new SyainBean();
		updateSyainBean.setSyainNo(syainNo);
		updateSyainBean.setSyainName(syainName);
		updateSyainBean.setPassword(password);
		updateSyainBean.setStatus(status);

		// 「Update」ボタンが押された場合
		if (UPDATE_BTN.equals(button)) {
			// 入力チェック
			updateSyainBean.setErrMsgSyainNo(SyainValidators.checkSyainNo(updateSyainBean.getSyainNo()));
			updateSyainBean.setErrMsgSyainName(SyainValidators.checkSyainName(updateSyainBean.getSyainName()));
			updateSyainBean.setErrMsgPassword(SyainValidators.checkPassword(updateSyainBean.getPassword()));
			updateSyainBean.setErrMsgStatus(SyainValidators.checkStatus(updateSyainBean.getStatus()));
			request.setAttribute("updateSyainBean", updateSyainBean);
			// 入力チェックエラーが発生した場合、入力画面に遷移
			if (updateSyainBean.isErrFlg()) {
				getServletContext().getRequestDispatcher(SYAIN_UPDATE_INPUT_JSP).forward(request, response);
				return;
			}
			// ロール情報を更新
			try {
				SyainDAO syainDAO = new SyainDAO();
				syainDAO.updateSyain(updateSyainBean);
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			// ロール情報をリクエストに設定し、完了画面に遷移
			request.setAttribute("updateSyainBean", updateSyainBean);
			getServletContext().getRequestDispatcher(SYAIN_UPDATE_COMP_JSP).forward(request, response);

			// 「Clear」ボタンが押された場合、入力画面に遷移
		} else {
			request.setAttribute("updateSyainBean", syainBean);
			getServletContext().getRequestDispatcher(SYAIN_UPDATE_INPUT_JSP).forward(request, response);
		}
	}
}
