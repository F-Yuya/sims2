package servlet.sikaku;

import static parameter.ButtonNames.*;
import static parameter.ContentPaths.*;
import static parameter.ErrorMessages.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.SikakuBean;
import dao.SikakuDAO;
import exception.DatabaseException;
import exception.SystemException;
import servlet.SIMSServlet;
import validator.SikakuValidators;

@WebServlet("/SikakuEntry")
public class SikakuEntry extends SIMSServlet {
	private static final long serialVersionUID = 1L;

	/** 資格情報登録入力画面を表示する。（GETメソッド） */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("sikakuBean", new SikakuBean());
		getServletContext().getRequestDispatcher(SIKAKU_ENTRY_INPUT_JSP).forward(request, response);
	}

	/** 資格情報登録機能でボタンが押された場合の処理。（POSTメソッド） */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータから資格情報を取得
		String sikakucode = request.getParameter("sikakucode");
		String sikakuname = request.getParameter("sikakuname");
		String sikakugroup = request.getParameter("sikakugroup");
		String button = request.getParameter("button");

		// 資格情報を生成
		SikakuBean sikakuBean = new SikakuBean();
		sikakuBean.setSikakucode(sikakucode);
		sikakuBean.setSikakuname(sikakuname);
		sikakuBean.setSikakugroup(sikakugroup);

		// 「Submit」ボタンが押された場合
		if (SUBMIT_BTN.equals(button)) {
			// 入力チェック
			sikakuBean.setErrMsgSikakucode(SikakuValidators.checkSikakuCode(sikakuBean.getSikakucode()));
			sikakuBean.setErrMsgSikakuname(SikakuValidators.checkSikakuName(sikakuBean.getSikakuname()));
			//sikakuBean.setErrMsgSikakugroup(SikakuValidators.checkSikakuGroup(sikakuBean.getSikakugroup()));
			// 入力チェックエラーが発生した場合、入力画面に遷移
			if (sikakuBean.isErrFlg()) {
				request.setAttribute("SikakuBean", sikakuBean);
				getServletContext().getRequestDispatcher(SIKAKU_ENTRY_INPUT_JSP).forward(request, response);
				return;
			}
			// 登録済みの資格情報の場合、入力画面に遷移
			try {
				SikakuDAO sikakuDAO = new SikakuDAO();
				if (sikakuDAO.isEntryed(sikakucode)) {
					sikakuBean.setErrMsgSikakucode(SIKAKUCODE_ISENTRYED);
					request.setAttribute("sikakuBean", sikakuBean);
					getServletContext().getRequestDispatcher(SIKAKU_ENTRY_INPUT_JSP).forward(request, response);
					return;
				}
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			// 資格情報をリクエストに設定し、確認画面に遷移
			request.setAttribute("sikakuBean", sikakuBean);
			getServletContext().getRequestDispatcher(SIKAKU_ENTRY_CONFIRM_JSP).forward(request, response);

			// 「Back」ボタンが押された場合
		} else if (BACK_BTN.equals(button)) {
			// 社員情報をリクエストに設定し、入力画面に遷移
			request.setAttribute("sikakuBean", sikakuBean);
			getServletContext().getRequestDispatcher(SIKAKU_ENTRY_INPUT_JSP).forward(request, response);

			// 「Entry」ボタンが押された場合
		} else if (ENTRY_BTN.equals(button)) {
			// 資格情報を登録
			try {
				SikakuDAO sikakuDAO = new SikakuDAO();
				sikakuDAO.entrySikaku(sikakuBean);
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			// 資格情報をリクエストに設定し、完了画面に遷移
			request.setAttribute("sikakuBean", sikakuBean);
			getServletContext().getRequestDispatcher(SIKAKU_ENTRY_COMP_JSP).forward(request, response);

			// 「Clear」ボタンが押された場合、入力画面に遷移
		} else {
			request.setAttribute("sikakuBean", new SikakuBean());
			getServletContext().getRequestDispatcher(SIKAKU_ENTRY_INPUT_JSP).forward(request, response);
		}
	}
}
