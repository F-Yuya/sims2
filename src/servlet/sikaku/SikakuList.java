package servlet.sikaku;

import static parameter.ContentPaths.*;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.SikakuBean;
import dao.SikakuDAO;
import exception.DatabaseException;
import exception.SystemException;
import servlet.SIMSServlet;

@WebServlet("/SikakuList")
public class SikakuList extends SIMSServlet {
	private static final long serialVersionUID = 1L;

	/** 資格情報一覧画面を表示する。（GETメソッド） */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ロールを取得
		String role = (String) request.getAttribute("role");
		// 資格テーブルを検索して社員情報を取得
		ArrayList<SikakuBean> sikakuBeanList;
		try {
			SikakuDAO sikakuDAO = new SikakuDAO();
			sikakuBeanList = sikakuDAO.getSikakuBeanList(role);
		} catch (SystemException | DatabaseException e) {
			// 例外が発生した場合、エラー画面に遷移
			e.printStackTrace();
			request.setAttribute("exMsg", e.getMessage());
			getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
			return;
		}
		request.setAttribute("sikakuBeanList", sikakuBeanList);
		getServletContext().getRequestDispatcher(SIKAKU_LIST_JSP).forward(request, response);
	}

}
