package servlet.sikaku;

import static parameter.ButtonNames.*;
import static parameter.ContentPaths.*;
import static parameter.ErrorMessages.*;
import static parameter.SyainParameters.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.SikakuBean;
import common.StringCommon;
import dao.SikakuDAO;
import exception.DatabaseException;
import exception.SystemException;
import servlet.SIMSServlet;
import validator.SikakuValidators;

@WebServlet("/SikakuUpdate")
public class SikakuUpdate extends SIMSServlet {
	private static final long serialVersionUID = 1L;

	/** 資格情報更新入力画面を表示する。（GETメソッド） */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String sikakucode = request.getParameter("sikakucode");
		SikakuBean sikakuBean;
		try {
			SikakuDAO sikakuDAO = new SikakuDAO();
			sikakuBean = sikakuDAO.getSikakuBean(sikakucode);

			// 存在しない資格または廃止した資格の場合、エラー画面に遷移
			if (StringCommon.cnvNull(sikakuBean.getSikakucode()).equals("") || RETIRE.equals(sikakuBean.getStatus())) {
				request.setAttribute("errMsg", NOTEXISTS_SYAIN);
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
		} catch (SystemException | DatabaseException e) {
			// 例外が発生した場合、エラー画面に遷移
			e.printStackTrace();
			request.setAttribute("exMsg", e.getMessage());
			getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
			return;
		}
		// 更新前の資格情報をセッションとリクエストに設定
		HttpSession session = request.getSession(false);
		session.setAttribute("sikakuBean", sikakuBean);
		request.setAttribute("updateSikakuBean", sikakuBean);
		getServletContext().getRequestDispatcher(SIKAKU_UPDATE_INPUT_JSP).forward(request, response);
	}

	/** 資格情報更新機能でボタンが押された場合の処理。（POSTメソッド） */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッションから更新前の資格情報を取得
		HttpSession session = request.getSession(false);
		SikakuBean sikakuBean = (SikakuBean) session.getAttribute("sikakuBean");
		// リクエストパラメータから更新する資格情報を取得
		String sikakucode = request.getParameter("sikakucode");
		String sikakuname = request.getParameter("sikakuname");
		String sikakugroup = request.getParameter("sikakugroup");
		String status = request.getParameter("status");
		String button = request.getParameter("button");

		// 資格情報を生成
		SikakuBean updateSikakuBean = new SikakuBean();
		updateSikakuBean.setSikakucode(sikakucode);
		updateSikakuBean.setSikakuname(sikakuname);
		updateSikakuBean.setSikakugroup(sikakugroup);
		updateSikakuBean.setStatus(status);

		// 「Update」ボタンが押された場合
		if (UPDATE_BTN.equals(button)) {
			// 入力チェック
			updateSikakuBean.setErrMsgSikakucode(SikakuValidators.checkSikakuCode(updateSikakuBean.getSikakucode()));
			updateSikakuBean.setErrMsgSikakuname(SikakuValidators.checkSikakuName(updateSikakuBean.getSikakuname()));
			//			updateSikakuBean.setErrMsgPassword(SikakuValidators.check(updateSikakuBean.getPassword()));
			updateSikakuBean.setErrMsgStatus(SikakuValidators.checkStatus(updateSikakuBean.getStatus()));
			request.setAttribute("updateSyainBean", updateSikakuBean);
			// 入力チェックエラーが発生した場合、入力画面に遷移
			if (updateSikakuBean.isErrFlg()) {
				getServletContext().getRequestDispatcher(SIKAKU_UPDATE_INPUT_JSP).forward(request, response);
				return;
			}
			// 資格情報を更新
			try {
				SikakuDAO sikakuDAO = new SikakuDAO();
				sikakuDAO.updateSikaku(updateSikakuBean);
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			// 資格情報をリクエストに設定し、完了画面に遷移
			request.setAttribute("updateSikakuBean", updateSikakuBean);
			getServletContext().getRequestDispatcher(SIKAKU_UPDATE_COMP_JSP).forward(request, response);

			// 「Clear」ボタンが押された場合、入力画面に遷移
		} else {
			request.setAttribute("updateSikakuBean", sikakuBean);
			getServletContext().getRequestDispatcher(SIKAKU_UPDATE_INPUT_JSP).forward(request, response);
		}
	}
}
