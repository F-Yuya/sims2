package servlet.skill;

import static parameter.ButtonNames.*;
import static parameter.ContentPaths.*;
import static parameter.ErrorMessages.*;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.LoginSession;
import bean.SikakuBean;
import bean.SyainBean;
import bean.SyainSkillBean;
import dao.SikakuDAO;
import dao.SkillDAO;
import dao.SyainDAO;
import exception.DatabaseException;
import exception.SystemException;
import servlet.SIMSServlet;

@WebServlet("/SkillEntry")
public class SkillEntry extends SIMSServlet {
	private static final long serialVersionUID = 1L;

	/** スキル情報登録入力画面を表示する。（GETメソッド） */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String role = (String) request.getAttribute("role");
		ArrayList<SyainBean> syainBeanList;
		ArrayList<SikakuBean> sikakuBeanList;
		try {
			SyainDAO syainDAO = new SyainDAO();
			SikakuDAO sikakuDAO = new SikakuDAO();
			syainBeanList = syainDAO.getSyainBeanList(role);
			sikakuBeanList = sikakuDAO.getSikakuBeanList(role);
		} catch (SystemException | DatabaseException e) {
			// 例外が発生した場合、エラー画面に遷移
			e.printStackTrace();
			request.setAttribute("exMsg", e.getMessage());
			getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
			return;
		}
		Date day = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String today = sdf.format(day);
		request.setAttribute("today", today);
		request.setAttribute("syainBeanList", syainBeanList);
		request.setAttribute("sikakuBeanList", sikakuBeanList);
		getServletContext().getRequestDispatcher(SKILL_ENTRY_INPUT_JSP).forward(request, response);
	}

	/** スキル情報登録機能でボタンが押された場合の処理。（POSTメソッド） */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータからスキル情報を取得
		String syainNo = request.getParameter("syainNo");
		String sikakucode = request.getParameter("sikakucode");
		String sikakudate = (String) request.getParameter("sikakudate");
		String button = request.getParameter("button");
		// 資格情報,社員情報を取得
		SyainBean syianBean = null;
		SikakuBean sikakuBean = null;
		try {
			SyainDAO syainDAO = new SyainDAO();
			SikakuDAO sikakuDAO = new SikakuDAO();
			syianBean = syainDAO.getSyainBean(syainNo);
			sikakuBean = sikakuDAO.getSikakuBean(sikakucode);
		} catch (DatabaseException | SystemException e1) {
			e1.printStackTrace();
		}

		// スキル情報を生成
		SyainSkillBean syainSkillBean = new SyainSkillBean();
		syainSkillBean.setSyainno(syianBean.getSyainNo());
		syainSkillBean.setSyainName(syianBean.getSyainName());
		syainSkillBean.setSikakucode(sikakuBean.getSikakucode());
		syainSkillBean.setSikakuName(sikakuBean.getSikakuname());
		syainSkillBean.setSikakudate(sikakudate);

		// 「Submit」ボタンが押された場合
		if (SUBMIT_BTN.equals(button)) {
			// 登録済みのスキル情報の場合、入力画面に遷移
			try {
				SkillDAO skillDAO = new SkillDAO();
				if (skillDAO.isEntryed(syainSkillBean.getSyainno(), syainSkillBean.getSikakucode())) {
					syainSkillBean.setErrMsgSyainNo(SKILL_ISENTRYED);
					HttpSession session = request.getSession();
					LoginSession loginSession = (LoginSession) session.getAttribute("loginSession");
					String role = loginSession.getRole();
					ArrayList<SyainBean> syainBeanList;
					ArrayList<SikakuBean> sikakuBeanList;
					try {
						SyainDAO syainDAO = new SyainDAO();
						SikakuDAO sikakuDAO = new SikakuDAO();
						syainBeanList = syainDAO.getSyainBeanList(role);
						sikakuBeanList = sikakuDAO.getSikakuBeanList(role);
					} catch (SystemException | DatabaseException e) {
						// 例外が発生した場合、エラー画面に遷移
						e.printStackTrace();
						request.setAttribute("exMsg", e.getMessage());
						getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
						return;
					}
					Date day = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String today = sdf.format(day);
					request.setAttribute("today", today);
					request.setAttribute("syainBeanList", syainBeanList);
					request.setAttribute("sikakuBeanList", sikakuBeanList);
					getServletContext().getRequestDispatcher(SKILL_ENTRY_INPUT_JSP).forward(request, response);
					return;
				}
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			// スキル情報をリクエストに設定し、確認画面に遷移
			request.setAttribute("syainSkillBean", syainSkillBean);
			getServletContext().getRequestDispatcher(SKILL_ENTRY_CONFIRM_JSP).forward(request, response);

			// 「Back」ボタンが押された場合
		} else if (BACK_BTN.equals(button)) {
			// スキル情報をリクエストに設定し、入力画面に遷移
			HttpSession session = request.getSession();
			LoginSession loginSession = (LoginSession) session.getAttribute("loginSession");
			String role = loginSession.getRole();
			ArrayList<SyainBean> syainBeanList;
			ArrayList<SikakuBean> sikakuBeanList;
			try {
				SyainDAO syainDAO = new SyainDAO();
				SikakuDAO sikakuDAO = new SikakuDAO();
				syainBeanList = syainDAO.getSyainBeanList(role);
				sikakuBeanList = sikakuDAO.getSikakuBeanList(role);
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			Date day = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String today = sdf.format(day);
			request.setAttribute("today", today);
			request.setAttribute("syainBeanList", syainBeanList);
			request.setAttribute("sikakuBeanList", sikakuBeanList);
			getServletContext().getRequestDispatcher(SKILL_ENTRY_INPUT_JSP).forward(request, response);

			// 「Entry」ボタンが押された場合
		} else if (ENTRY_BTN.equals(button)) {
			// スキル情報を登録
			try {
				SkillDAO skillDAO = new SkillDAO();
				skillDAO.entrySkill(syainSkillBean);
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			// スキル情報をリクエストに設定し、完了画面に遷移
			request.setAttribute("syainSkillBean", syainSkillBean);
			getServletContext().getRequestDispatcher(SKILL_ENTRY_COMP_JSP).forward(request, response);

			// 「Clear」ボタンが押された場合、入力画面に遷移
		} else {
			HttpSession session = request.getSession();
			LoginSession loginSession = (LoginSession) session.getAttribute("loginSession");
			String role = loginSession.getRole();
			ArrayList<SyainBean> syainBeanList;
			ArrayList<SikakuBean> sikakuBeanList;
			try {
				SyainDAO syainDAO = new SyainDAO();
				SikakuDAO sikakuDAO = new SikakuDAO();
				syainBeanList = syainDAO.getSyainBeanList(role);
				sikakuBeanList = sikakuDAO.getSikakuBeanList(role);
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			Date day = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String today = sdf.format(day);
			request.setAttribute("today", today);
			request.setAttribute("syainBeanList", syainBeanList);
			request.setAttribute("sikakuBeanList", sikakuBeanList);
			getServletContext().getRequestDispatcher(SKILL_ENTRY_INPUT_JSP).forward(request, response);
		}
	}
}
