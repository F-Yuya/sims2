package servlet.skill;

import static parameter.ContentPaths.*;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.LoginSession;
import bean.SyainSkillBean;
import dao.SkillDAO;
import exception.DatabaseException;
import exception.SystemException;
import servlet.SIMSServlet;

@WebServlet("/SkillList")
public class SkillList extends SIMSServlet {
	private static final long serialVersionUID = 1L;

	/** スキル情報一覧画面を表示する。（GETメソッド） */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ロールと社員番号を取得
		String role = (String) request.getAttribute("role");
		HttpSession session = request.getSession();
		LoginSession loginSession = (LoginSession) session.getAttribute("loginSession");
		String syainNo = loginSession.getSyainNo();
		// スキルテーブルを検索してスキル情報を取得
		ArrayList<SyainSkillBean> syainSkillBeanList;

		try {
			SkillDAO skillDAO = new SkillDAO();
			syainSkillBeanList = skillDAO.getSyainSkillBeanList(role, syainNo);
		} catch (SystemException | DatabaseException e) {
			// 例外が発生した場合、エラー画面に遷移
			e.printStackTrace();
			request.setAttribute("exMsg", e.getMessage());
			getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
			return;
		}
		request.setAttribute("syainSkillBeanList", syainSkillBeanList);
		getServletContext().getRequestDispatcher(SKILL_LIST_JSP).forward(request, response);
	}

}
