package servlet.skill;

import static parameter.ButtonNames.*;
import static parameter.ContentPaths.*;
import static parameter.ErrorMessages.*;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.SyainSkillBean;
import dao.SkillDAO;
import exception.DatabaseException;
import exception.SystemException;
import servlet.SIMSServlet;

@WebServlet("/SkillUpdate")
public class SkillUpdate extends SIMSServlet {
	private static final long serialVersionUID = 1L;

	/** スキル情報更新入力画面を表示する。（GETメソッド） */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String syainNo = request.getParameter("syainNo");
		String sikakucode = request.getParameter("sikakucode");
		SyainSkillBean syainSkillBean;
		try {
			SkillDAO skillDAO = new SkillDAO();
			// 存在しないスキルの場合、エラー画面に遷移
			if (!skillDAO.isEntryed(syainNo, sikakucode)) {
				request.setAttribute("errMsg", NOTEXISTS_SKILL);
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
		} catch (SystemException | DatabaseException e) {
			// 例外が発生した場合、エラー画面に遷移
			e.printStackTrace();
			request.setAttribute("exMsg", e.getMessage());
			getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
			return;
		}
		try {
			SkillDAO skillDAO2 = new SkillDAO();
			syainSkillBean = skillDAO2.getSyainSkillBean(syainNo, sikakucode);
		} catch (SystemException | DatabaseException e) {
			e.printStackTrace();
			request.setAttribute("exMsg", e.getMessage());
			getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
			return;
		}

		// 更新前の社員情報をセッションとリクエストに設定
		HttpSession session = request.getSession(false);
		Date day = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String today = sdf.format(day);
		request.setAttribute("today", today);
		session.setAttribute("syainSkillBean", syainSkillBean);
		request.setAttribute("updatesyainSkillBean", syainSkillBean);
		getServletContext().getRequestDispatcher(SKILL_UPDATE_INPUT_JSP).forward(request, response);
	}

	/** 社員情報更新機能でボタンが押された場合の処理。（POSTメソッド） */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッションから更新前の社員情報を取得
		HttpSession session = request.getSession(false);
		SyainSkillBean syainSkillBean = (SyainSkillBean) session.getAttribute("syainSkillBean");
		// リクエストパラメータから更新する社員情報を取得
		String syainNo = request.getParameter("syainNo");
		String syainName = request.getParameter("syainName");
		String sikakucode = request.getParameter("sikakucode");
		String sikakuname = request.getParameter("sikakuname");
		String sikakudate = request.getParameter("sikakudate");
		String button = request.getParameter("button");

		System.out.println(button);
		// 社員情報を生成
		SyainSkillBean updateSyainSkillBean = new SyainSkillBean();
		updateSyainSkillBean.setSyainno(syainNo);
		updateSyainSkillBean.setSyainName(syainName);
		updateSyainSkillBean.setSikakucode(sikakucode);
		updateSyainSkillBean.setSikakuName(sikakuname);
		updateSyainSkillBean.setSikakudate(sikakudate);

		// 「Update」ボタンが押された場合
		if (UPDATE_BTN.equals(button)) {
			// 入力チェック

			request.setAttribute("updateSyainSkillBean", updateSyainSkillBean);
			// 入力チェックエラーが発生した場合、入力画面に遷移
			if (updateSyainSkillBean.isErrFlg()) {
				getServletContext().getRequestDispatcher(SYAIN_UPDATE_INPUT_JSP).forward(request, response);
				return;
			}
			// 社員情報を更新
			try {
				SkillDAO skillDAO = new SkillDAO();
				skillDAO.updateSkill(updateSyainSkillBean);
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			// 社員情報をリクエストに設定し、完了画面に遷移
			request.setAttribute("updateSyainSkillBean", updateSyainSkillBean);
			getServletContext().getRequestDispatcher(SKILL_UPDATE_COMP_JSP).forward(request, response);

			// 「Delete」ボタンが押された場合
		} else if (DELETE_BTN.equals(button)) {

			// 社員情報を削除
			try {
				SkillDAO skillDAO = new SkillDAO();
				skillDAO.deleteSkill(updateSyainSkillBean);
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			// 社員情報をリクエストに設定し、完了画面に遷移
			//			request.setAttribute("updateSyainSkillBean", updateSyainSkillBean);
			getServletContext().getRequestDispatcher(SKILL_UPDATE_COMP_JSP).forward(request, response);

			// 「Clear」ボタンが押された場合、入力画面に遷移
		} else {
			request.setAttribute("updateSyainSkillBean", syainSkillBean);
			getServletContext().getRequestDispatcher(SKILL_UPDATE_INPUT_JSP).forward(request, response);
		}
	}
}
