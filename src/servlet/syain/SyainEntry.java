package servlet.syain;

import static parameter.ButtonNames.*;
import static parameter.ContentPaths.*;
import static parameter.ErrorMessages.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.SyainBean;
import dao.SyainDAO;
import dao.SyainRolesDAO;
import exception.DatabaseException;
import exception.SystemException;
import servlet.SIMSServlet;
import validator.SyainValidators;

@WebServlet("/SyainEntry")
public class SyainEntry extends SIMSServlet {
	private static final long serialVersionUID = 1L;

	/** 社員情報登録入力画面を表示する。（GETメソッド） */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("syainBean", new SyainBean());
		getServletContext().getRequestDispatcher(SYAIN_ENTRY_INPUT_JSP).forward(request, response);
	}

	/** 社員情報登録機能でボタンが押された場合の処理。（POSTメソッド） */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータから社員情報を取得
		String syainNo = request.getParameter("syainNo");
		String syainName = request.getParameter("syainName");
		String password = request.getParameter("password");
		String button = request.getParameter("button");

		// 社員情報を生成
		SyainBean syainBean = new SyainBean();
		syainBean.setSyainNo(syainNo);
		syainBean.setSyainName(syainName);
		syainBean.setPassword(password);

		// 「Submit」ボタンが押された場合
		if (SUBMIT_BTN.equals(button)) {
			// 入力チェック
			syainBean.setErrMsgSyainNo(SyainValidators.checkSyainNo(syainBean.getSyainNo()));
			syainBean.setErrMsgSyainName(SyainValidators.checkSyainName(syainBean.getSyainName()));
			syainBean.setErrMsgPassword(SyainValidators.checkPassword(syainBean.getPassword()));
			// 入力チェックエラーが発生した場合、入力画面に遷移
			if (syainBean.isErrFlg()) {
				request.setAttribute("syainBean", syainBean);
				getServletContext().getRequestDispatcher(SYAIN_ENTRY_INPUT_JSP).forward(request, response);
				return;
			}
			// 登録済みの社員情報の場合、入力画面に遷移
			try {
				SyainDAO syainDAO = new SyainDAO();
				if (syainDAO.isEntryed(syainNo)) {
					syainBean.setErrMsgSyainNo(SYAINNO_ISENTRYED);
					request.setAttribute("syainBean", syainBean);
					getServletContext().getRequestDispatcher(SYAIN_ENTRY_INPUT_JSP).forward(request, response);
					return;
				}
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			// 社員情報をリクエストに設定し、確認画面に遷移
			request.setAttribute("syainBean", syainBean);
			getServletContext().getRequestDispatcher(SYAIN_ENTRY_CONFIRM_JSP).forward(request, response);

			// 「Back」ボタンが押された場合
		} else if (BACK_BTN.equals(button)) {
			// 社員情報をリクエストに設定し、入力画面に遷移
			request.setAttribute("syainBean", syainBean);
			getServletContext().getRequestDispatcher(SYAIN_ENTRY_INPUT_JSP).forward(request, response);

			// 「Entry」ボタンが押された場合
		} else if (ENTRY_BTN.equals(button)) {
			// 社員情報とロール情報を登録
			try {
				SyainDAO syainDAO = new SyainDAO();
				syainDAO.entrySyain(syainBean);
				SyainRolesDAO syainRolesDAO = new SyainRolesDAO();
				syainRolesDAO.entrySyainRole(syainBean.getSyainNo());
			} catch (SystemException | DatabaseException e) {
				// 例外が発生した場合、エラー画面に遷移
				e.printStackTrace();
				request.setAttribute("exMsg", e.getMessage());
				getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
				return;
			}
			// 社員情報をリクエストに設定し、完了画面に遷移
			request.setAttribute("syainBean", syainBean);
			getServletContext().getRequestDispatcher(SYAIN_ENTRY_COMP_JSP).forward(request, response);

			// 「Clear」ボタンが押された場合、入力画面に遷移
		} else {
			request.setAttribute("syainBean", new SyainBean());
			getServletContext().getRequestDispatcher(SYAIN_ENTRY_INPUT_JSP).forward(request, response);
		}
	}
}
