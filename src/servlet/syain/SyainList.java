package servlet.syain;

import static parameter.ContentPaths.*;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.SyainBean;
import dao.SyainDAO;
import exception.DatabaseException;
import exception.SystemException;
import servlet.SIMSServlet;

@WebServlet("/SyainList")
public class SyainList extends SIMSServlet {
	private static final long serialVersionUID = 1L;

	/** 社員情報一覧画面を表示する。（GETメソッド） */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ロールを取得
		String role = (String) request.getAttribute("role");
		// 社員テーブルを検索して社員情報を取得
		ArrayList<SyainBean> syainBeanList;
		try {
			SyainDAO syainDAO = new SyainDAO();
			syainBeanList = syainDAO.getSyainBeanList(role);
		} catch (SystemException | DatabaseException e) {
			// 例外が発生した場合、エラー画面に遷移
			e.printStackTrace();
			request.setAttribute("exMsg", e.getMessage());
			getServletContext().getRequestDispatcher(ERROR_JSP).forward(request, response);
			return;
		}
		request.setAttribute("syainBeanList", syainBeanList);
		getServletContext().getRequestDispatcher(SYAIN_LIST_JSP).forward(request, response);
	}

}
