/**
 * SampleValidators.java  サンプル情報バリデーションチェッククラス
 */
package validator;

import static parameter.ErrorMessages.*;
import static parameter.SikakuParameters.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SampleValidators {

	// 正規表現（半角数字）
	private static final String MATCH_HANKAKUSUU = "^[0-9]+$";
	// 正規表現（半角英数字）
	private static final String MATCH_HANKAKUEISUU = "^[a-zA-Z0-9]+$";

	public static String checkSikakuGroup(String sikakuGroup) {
		if (!KOKKASHIKEN.equals(sikakuGroup) && !JKEN.equals(sikakuGroup) && !VENDOR.equals(sikakuGroup)) {
			return SIKAKUGROUP_REQUIRED;
		} else {
			/* 正常 */
			return "";
		}
	}

	/** 社員番号をチェックし、エラーメッセージを返す
	 *
	 * @param syainNo
	 * @return エラーメッセージ */
	public static String checkSyainNo(String syainNo) {
		if (syainNo.isEmpty()) {
			/* 必須チェック */
			return GETSYAINNO_REQUIRED;
		} else if (syainNo.length() != 5) {
			/* 桁数チェック */
			return GETSYAINNO_REQUIRED;
		} else if (!syainNo.matches(MATCH_HANKAKUEISUU)) {
			/* 文字種チェック */
			return GETSYAINNO_REQUIRED;
		} else {
			/* 正常 */
			return "";
		}
	}

	public static String checkSikakuCode(String sikakuCode) {
		if (sikakuCode.isEmpty()) {
			/* 必須チェック */
			return GETSIKAKUCODE_REQUIRED;
		} else if (sikakuCode.length() != 3) {
			/* 桁数チェック */
			return GETSIKAKUCODE_REQUIRED;
		} else if (!sikakuCode.matches(MATCH_HANKAKUSUU)) {
			/* 文字種チェック */
			return GETSIKAKUCODE_REQUIRED;
		} else {
			/* 正常 */
			return "";
		}
	}

	public static String checkSikakuDate(String sikakuDate) {
		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		Date indication = null;
		try {
			indication = format.parse(sikakuDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return GETSIKAKUDATE_REQUIRED;
		}
		if (sikakuDate.isEmpty()) {
			/* 必須チェック */
			return GETSIKAKUDATE_REQUIRED;
		} else if (now.getTime() < indication.getTime()) {
			/* 未来日チェック */
			return GETSIKAKUDATE_MISTAKE;
		} else {
			/* 正常 */
			return "";
		}
	}

}
