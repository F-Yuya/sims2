/**
 * SyainValidators.java  資格情報バリデーションチェッククラス
 */
package validator;

import static parameter.ErrorMessages.*;
import static parameter.SikakuParameters.*;

public class SikakuValidators {

	// 正規表現（半角英数字）
	private static final String MATCH_HANKAKUEISUU = "^[a-zA-Z0-9]+$";

	/**
	 * 資格番号をチェックし、エラーメッセージを返す
	 *
	 * @param sikakucode
	 * @return エラーメッセージ
	 */
	public static String checkSikakuCode(String sikakucode) {
		if (sikakucode.isEmpty()) {
			/* 必須チェック */
			return SIKAKUCODE_REQUIRED;
		} else if (sikakucode.length() != 3) {
			/* 桁数チェック */
			return SIKAKUCODE_DIGIT;
		} else if (!sikakucode.matches(MATCH_HANKAKUEISUU)) {
			/* 文字種チェック */
			return SIKAKUCODE_DIGIT;
		} else {
			/* 正常 */
			return "";
		}
	}

	/**
	 * 資格名をチェックし、エラーメッセージを返す
	 *
	 * @param sikakuname
	 * @return エラーメッセージ
	 */
	public static String checkSikakuName(String sikakuname) {
		if (sikakuname.isEmpty()) {
			/* 必須チェック */
			return SIKAKUNAME_REQUIRED;
		} else if (sikakuname.length() > 30) {
			/* 桁数チェック */
			return SIKAKUNAME_DIGIT;
		} else {
			/* 正常 */
			return "";
		}
	}

	//	/**
	//	 * 資格区分をチェックし、エラーメッセージを返す
	//	 *
	//	 * @param sikakugroup
	//	 * @return エラーメッセージ
	//	 */
	//	public static String checkSikakuGroup(String sikakugroup) {
	//		if (sikakugroup.isEmpty()) {
	//			/* 必須チェック */
	//			return SIKAKUGROUP_REQUIRED;
	//		} else if (sikakugroup.length() > 20) {
	//			/* 桁数チェック */
	//			return SIKAKUGROUP_DIGIT;
	//		} else if (!sikakugroup.matches(MATCH_HANKAKUEISUU)) {
	//			/* 文字種チェック */
	//			return SIKAKUGROUP_DIGIT;
	//		} else {
	//			/* 正常 */
	//			return "";
	//		}
	//	}

	/** 在籍状況をチェックし、エラーメッセージを返す
	 *
	 * @param status
	 * @return エラーメッセージ */
	public static String checkStatus(String status) {
		if (!NOT_DEL.equals(status) && !DEL.equals(status)) {
			return SIKAKU_STATUS_REQUIRED;
		} else {
			/* 正常 */
			return "";
		}
	}
}
