/**
 * SyainValidators.java  社員情報バリデーションチェッククラス
 */
package validator;

import static parameter.ErrorMessages.*;
import static parameter.SyainParameters.*;

public class SyainValidators {

	// 正規表現（半角英数字）
	private static final String MATCH_HANKAKUEISUU = "^[a-zA-Z0-9]+$";

	/** 社員番号をチェックし、エラーメッセージを返す
	 *
	 * @param syainNo
	 * @return エラーメッセージ */
	public static String checkSyainNo(String syainNo) {
		if (syainNo.isEmpty()) {
			/* 必須チェック */
			return SYAINNO_REQUIRED;
		} else if (syainNo.length() != 5) {
			/* 桁数チェック */
			return SYAINNO_DIGIT;
		} else if (!syainNo.matches(MATCH_HANKAKUEISUU)) {
			/* 文字種チェック */
			return SYAINNO_DIGIT;
		} else {
			/* 正常 */
			return "";
		}
	}

	/** 社員名をチェックし、エラーメッセージを返す
	 *
	 * @param syainName
	 * @return エラーメッセージ */
	public static String checkSyainName(String syainName) {
		if (syainName.isEmpty()) {
			/* 必須チェック */
			return SYAINNAME_REQUIRED;
		} else if (syainName.length() > 25) {
			/* 桁数チェック */
			return SYAINNAME_DIGIT;
		} else {
			/* 正常 */
			return "";
		}
	}

	/** パスワードをチェックし、エラーメッセージを返す
	 *
	 * @param password
	 * @return エラーメッセージ */
	public static String checkPassword(String password) {
		if (password.isEmpty()) {
			/* 必須チェック */
			return PASSWORD_REQUIRED;
		} else if (password.length() < 4 || password.length() > 10) {
			/* 桁数チェック */
			return PASSWORD_DIGIT;
		} else if (!password.matches(MATCH_HANKAKUEISUU)) {
			/* 文字種チェック */
			return PASSWORD_DIGIT;
		} else {
			/* 正常 */
			return "";
		}
	}

	/** 在籍状況をチェックし、エラーメッセージを返す
	 *
	 * @param status
	 * @return エラーメッセージ */
	public static String checkStatus(String status) {
		if (!NOT_RETIRE.equals(status) && !RETIRE.equals(status)) {
			return STATUS_REQUIRED;
		} else {
			/* 正常 */
			return "";
		}
	}
}
